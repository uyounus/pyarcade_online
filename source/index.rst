*****************************************************
Welcome to Pyarcade Online (Squad 2)'s documentation!
*****************************************************
.. toctree::
   :maxdepth: 2
   :caption: Contents:

Doumentation for Main Module
============================

Pyarcade main
*************
.. automodule:: pyarcade.start
   :members:

Pyarcade Database
*****************
.. automodule:: pyarcade.database
   :members:

Pyarcade View
*************
.. automodule:: pyarcade.view
   :members:

Pyarcade State
**************
.. automodule:: pyarcade.state
   :members:

Pyarcade Game State
*******************
.. automodule:: pyarcade.game_state
   :members:

Pyarcade Game Manager
*********************
.. automodule:: pyarcade.game_manager
   :members:

Pyarcade Account Manager
************************
.. automodule:: pyarcade.accountManager
   :members:

Pyarcade Login Manager
**********************
.. automodule:: pyarcade.login_manager
   :members:

Pyarcade Inbox Manager
**********************
.. automodule:: pyarcade.inbox_manager
   :members:

Pyarcade Input System
*********************
.. automodule:: pyarcade.input_system
   :members:


Documentation for Games
=======================

Pyarcade Connect4
*****************
.. automodule:: pyarcade.connect4.connect4
   :members:

Pyarcade Go Fish
****************
.. automodule:: pyarcade.go_fish.go_fish
   :members:

Pyarcade Mastermind
*******************
.. automodule:: pyarcade.mastermind.mastermind
   :members:

Pyarcade Minesweeper
********************
.. automodule:: pyarcade.minesweeper.minesweeper
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
