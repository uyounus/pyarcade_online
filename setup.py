from setuptools import setup, find_packages

setup(
    name='pyarcade',
    version='3.0.0',
    description='An online game arcade.',
    author='Ophir Gal, Andrew Gordon, Usama Younus, Stephen Nelson Jr.',
    author_email='ophirgal2@gmail.com, gxrdonandrew@gmail.com, '
                 'usama.younus01@gmail.com, snelson9@terpmail.umd.edu',
    license='MIT',
    packages=find_packages(),
    entry_points={
        "console_scripts": ["pyarcade = pyarcade.start:run_pyarcade"]},
    test_suite="tests",
    include_package_data=True,
    install_requires=[]
)
