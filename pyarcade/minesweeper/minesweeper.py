from typing import Optional, List, TypeVar
import random
from pyarcade.input_system import get_minesweeper_position
from pyarcade.game_state import GameState


class Minesweeper(GameState):
    """
    A class representing a single(!) Minesweeper game session.
        The game is slightly simplified since it doesn't allow to flag squares;
        secondly, it does not have a fixed number of mines, mine quantity is
        somewhat random, but higher difficulty means likelihood of higher amount.
        Thirdly, it does not have the feature where if a zero-square is clicked,
        its neighboring zero-squares also get clicked, and so on.
    """
    welcome_message = "***************************\n" \
                      "* Welcome to Minesweeper! *\n" \
                      "***************************\n"

    class Square:
        """
        A class representing a single square on a Minesweeper grid
        """

        def __init__(self, is_mine: bool, value: int = 0, hidden: bool = True):
            """
            :param is_mine: bool: signifies whether this square is 
                a mine.
            :param value: int: a number between 0 and 8 of surrounding 
                mines.
            :param hidden: bool: signifies if it should be visible on 
                the grid.

            Initializes a single square on a Minesweeper grid
            """
            self.is_mine = is_mine
            self.value = value
            self.hidden = hidden
            self.flagged = False

        def unhide(self) -> None:
            """
            :return: None.

            "Unhides" the square.
            """
            self.hidden = False

        def toggle_flag(self) -> None:
            """
            :return: None.

            Toggles the square's flag.
            """
            self.flagged = not self.flagged

        def __str__(self) -> str:
            """
            :return:
                str: a string representation of this square.

                '⚑' denotes a flagged square;
                '?' denotes a hidden square;
                '*' denotes a mine;
                '1'...'9' denotes the number of mines surrounding the square;
                '_' denotes a square with no mines around it.
            """
            if self.flagged:
                return '⚑'
            elif self.hidden:
                return '?'
            elif self.is_mine:
                return '*'
            else:
                return '_' if self.value == 0 else str(self.value)

    def __init__(self, height: int = 10, width: int = 10,
                 difficulty: str = 'easy'):
        """
        :param height: int: number of rows on the grid.
        :param width: int: number of columns on the grid.
        :param difficulty: str: either 'easy', 'medium', or 'hard'. The higher
            the difficulty, the more mines the grid is likely to have.

        Initializes a Minesweeper game with a new (hidden) 10x10 grid.
        """
        d = difficulty  # shorthand for difficulty
        if d != 'easy' and d != 'medium' and d != 'hard':
            raise ValueError(
                "Invalid difficulty! use 'easy', 'medium', or 'hard'.")
        self.height = height
        self.width = width
        self.difficulty = difficulty
        self.grid = self.generate_random_grid(self.height, self.width)
        self.show_welcome = True  # flag for "welcome" message
        self.game_state = 'GAME IN SESSION'
        self.score = 0

    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        return self.score

    @staticmethod
    def count_surrounding_mines(binary_grid: List[List[str]], row: int,
                                col: int) -> int:
        """
        :param binary_grid: List[List[str]]: 2D grid of 'mine' and 'no mine'.
        :param row: int: row of current square.
        :param col: int: column of current square.
        :return: count int: number of mines arround binary_grid[row][col].

        Counts the mines around the square at binary_grid[row][col].
        """
        count = 0
        for row_idx in range(row - 1, row + 2):
            for col_idx in range(col - 1, col + 2):
                if 0 <= row_idx < len(binary_grid) \
                        and 0 <= col_idx < len(binary_grid[0]) \
                        and binary_grid[row_idx][col_idx] == 'mine':
                    count += 1

        return count

    def generate_random_grid(self, height: int, width: int) -> List[
        List[Square]]:
        """
        :param height: int: number of rows on the grid.
        :param width: int: number of columns on the grid.
        :return: List[List[Minesweeper.Square]]: a 2D grid of Square objects.

        Generates a random Minesweeper grid of shape (height, width).
        """
        chance_of_mine = 0.1
        if self.difficulty == 'medium':
            chance_of_mine = 0.12
        elif self.difficulty == 'hard':
            chance_of_mine = 0.17
        # create binary grid of 'mine' and 'no mine'
        binary_grid = [random.choices(population=['mine', 'no mine'],
                                      weights=[chance_of_mine,
                                               1 - chance_of_mine],
                                      k=width)
                       for _ in range(height)]
        # create blank grid to be populated with Square objects
        squares_grid = [[None for _ in range(width)] for _ in range(height)]
        # populate grid with the appropriate Square objects
        self.populate_grid_with_squares(binary_grid, height, width,
                                        squares_grid)

        return squares_grid

    def populate_grid_with_squares(self, binary_grid: List[List[str]],
                                   height: int, width: int,
                                   squares_grid: List[List[None]]) -> None:
        """
        :param binary_grid: 2D grid of "mine" and "no mine" values.
        :param height: int: height of the grid.
        :param squares_grid: 2D squares_grid to fill.
        :param width: int: width of the grid.
        :return: None.

        Populates grid with the appropriate Square objects depending on a
            given "binary grid" of boolean values.
        """
        for row in range(height):
            for col in range(width):
                if binary_grid[row][col] == 'mine':
                    squares_grid[row][col] = Minesweeper.Square(is_mine=True)
                else:
                    count = Minesweeper.count_surrounding_mines(binary_grid,
                                                                row, col)
                    squares_grid[row][col] = Minesweeper.Square(is_mine=False,
                                                                value=count)

    def only_mines_are_hidden(self) -> bool:
        """
        :return: bool: True if all none-mine squares have been revealed on
            the grid, and False otherwise.

        Checks whether all none-mine squares have been revealed on the grid.
        """
        for row in range(self.height):
            for col in range(self.width):
                square = self.grid[row][col]
                if square.hidden and not square.is_mine:
                    return False
        return True

    def click_square(self, row: int, col: int) -> None:
        """
        :param row: int: row of current square.
        :param col: int: column of current square.
        :return: None.

        Clicks a square and modifies the grid and game state accordingly:
            * If a number is clicked, it is revealed.
            * If a mine is clicked, the game is over, the grid is revealed.

        Note: this assumes 0-indexing AND valid coordinates.

        """
        square = self.grid[row][col]
        if square.is_mine:
            self.game_state = "GAME OVER"
            self.reveal_grid()
            return
        self.reveal_clicked(row, col)
        if self.only_mines_are_hidden():
            self.score += 1
            self.game_state = "GAME WON"

    def reveal_grid(self) -> None:
        """
        :return: Nones.

        Reveals all squares on the grid.
        """
        for row_idx in range(self.height):
            for col_idx in range(self.width):
                self.grid[row_idx][col_idx].unhide()

    def reset_game(self, difficulty: str = 'easy') -> None:
        """
        :param difficulty: str: either easy, medium, or hard. The higher the
            difficulty, the more mines the grid is likely to have.
        :return: None.

        Resets the game by creating a fresh new grid.
        """
        self.__init__(difficulty=difficulty)

    def __str__(self) -> str:
        """
        :return: str: a string representation of the current grid.
        """
        grid_str = " "
        for i in range(0, self.width):
            grid_str += "  " + str(i + 1)
        grid_str += "\n"

        for row in range(self.height):
            if row == 9:
                grid_str += str(row + 1)
            else:
                grid_str += str(row + 1)
            for col in range(self.width):
                if row == 9 and col == 0:
                    grid_str += " " + str(self.grid[row][col])
                else:
                    grid_str += "  " + str(self.grid[row][col])
            grid_str += '\n'

        return grid_str

    def input(self, user_input) -> None:
        """
        :param user_input: str: text input provided by the user.
        :return: None.

        Processes the provided user input.
        """
        self.show_welcome = False

        if user_input == 'reset':  # "reset the current game/session"
            self.reset_game()
        elif user_input == "hard" or user_input == "medium" or user_input == "easy":
            self.__init__(self.height, self.width, user_input)
        elif self.game_state == 'GAME IN SESSION':
            row, col, flag = get_minesweeper_position(user_input, self.height,
                                                      self.width)
            if (row, col) != (-1, -1):  # invalid input will be ignored
                if flag:
                    self.toggle_flag_of_square(row=row, col=col)
                elif not self.grid[row][col].flagged:
                    self.click_square(row=row, col=col)

    def output(self) -> str:
        """
        :return: str: the output to display.

        Provide output to the user (reflects the game state).
        """

        instructs = "\nCurrent Score: " + str(self.score) + "\n\n" \
             "Difficutly: " + self.difficulty + "\nTo change " \
             "the difficulty and reset the game, enter " \
             "\"easy\", \"medium\", or \"hard\"." + "\n" \
             "Please Note: Changing the difficulty simply" \
             " increases/decreases your chances of hitting a mine.\n"

        return (Minesweeper.welcome_message if self.show_welcome else '') + \
               self.game_state + '\n' + self.__str__() + \
               (instructs if self.game_state == 'GAME IN SESSION' else '') + \
               '\nEnter \"reset\" to reset the current session.'

    def toggle_flag_of_square(self, row, col) -> None:
        """
        :param row: int: row of square to flag/"unflag".
        :param col: int: col of square to flag/"unflag".
        :return: None.

        Either flags or "unflags" the provided square.
        """
        self.grid[row][col].toggle_flag()

    def reveal_clicked(self, row, col) -> None:
        """
        :param row: int: row of square around which to reveal.
        :param col: int: col of square around which to reveal.
        :return: None.

        Reveals squares and the ones around them (if needed) recursively.
        """
        if self.grid[row][col].hidden:
            self.grid[row][col].unhide()
            if self.grid[row][col].value == 0:
                for row_idx in range(max(row - 1, 0),
                                     min(row + 2, self.height)):
                    for col_idx in range(max(col - 1, 0),
                                         min(col + 2, self.width)):
                        if row_idx != row or col_idx != col:
                            self.reveal_clicked(row_idx, col_idx)
