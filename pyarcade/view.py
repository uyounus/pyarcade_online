import curses
import traceback

from pyarcade.database import Database
from pyarcade.game_manager import GameManager
from pyarcade.accountManager import AccountManager
from pyarcade.login_manager import LoginManager
from pyarcade.inbox_manager import InboxManager
from pathlib import Path

can_play_music = True
try:
    import simpleaudio
except Exception:
    can_play_music = False

class View:
    """
    A UI class for PyArcade.
    """
    CONNECT4 = '1'
    GOFISH = '2'
    MASTERMIND = '3'
    MINESWEEPER = '4'
    GAME_NUM_TO_NAME = {CONNECT4: 'Connect4',
                        GOFISH: 'Go Fish',
                        MASTERMIND: 'Mastermind',
                        MINESWEEPER: 'Minesweeper'}

    def __init__(self, testing: bool = False, test_input: list = []):
        """
        :param testing: bool: whether currently testing.
        :param test_input: list: the list of string inputs to read from

        Initializes a view with a GameManager instance, AccountManager
            instance, and LoginManager instance.
        """
        self.window = None
        self.game_manager = GameManager()
        self.creation_manager = AccountManager()
        self.login_manager = LoginManager()
        self.testing = testing
        self.test_input = test_input
        self.test_output = ""
        self.song_path = str(Path(__file__).parent) + \
            r'\resources\fatrat_jackpot.wav'
        self.play_obj = None
        self.wave_obj = None
        if can_play_music and not self.testing:
            self.wave_obj = simpleaudio.WaveObject.from_wave_file(
                self.song_path)

    def init_window(self) -> None:
        """
        :return: None.

        Initializes a curses window with the desired settings.
        """
        self.window = curses.initscr()  # initialize curses window
        self.clear()  # clear window
        self.window.scrollok(True)  # ensure window scrolls
        self.window.idlok(True)  # ensure window scrolls
        curses.nocbreak()  # ensure usage of [Enter] key
        # user input is echoed to the user by default

    def addstr(self, string) -> None:
        """
        :param string: the string to add.
        :return: None.

        Adds string either to test_output or to curses UI.
        """
        if self.testing:
            self.test_output += string
        else:
            self.window.addstr(string)

    def getstr(self) -> str:
        """
        :return: str: a string input.

        Get input either from curses or from test_input
        """
        if self.testing:
            string = self.test_input[0]
            del self.test_input[0]  # consume current input
            return string
        else:
            return self.window.getstr().decode(self.window.encoding)

    def clear(self) -> None:
        """
        Clears the screen or the test_output variable
        """
        if self.testing:
            self.test_output = ''
        else:
            self.window.clear()

    def display_main_menu(self) -> None:
        """
        :return: None.

        Displays the PyArcade main menu.
        """
        self.clear()  # clear window
        self.addstr("************************\n")
        self.addstr("* Welcome to PyArcade! *\n")
        self.addstr("************************\n\n")
        self.addstr("The following games are available:\n")
        idx = 1
        for game_name in self.game_manager.game_list():
            self.addstr(str(idx) + ". " + game_name + "\n")
            idx += 1
        self.addstr('\n')
        if not self.login_manager.is_logged_in():
            self.addstr('No user is currently signed in. Enter '
                        '"login" '
                        'to log into your account or enter "create" '
                        'to create one.\n')
            self.addstr('To play as a guest, select a game by '
                        'entering its number or enter '
                        '"quit" to quit PyArcade.\n')
        else:
            self.addstr('Hi ' + self.login_manager.get_curr_user() +
                        '!\n')
            self.addstr('Start a game by entering its number or enter '
                        '"quit" to quit PyArcade.\n')
            self.addstr('Enter \"logout\" to log out of your account.\n\n')
            self.addstr('Enter \"scores\" to see your scores.\n')
            self.addstr('Enter \"delete history\" to delete all '
                        'of your scores and logged time playing Pyarcade.\n')
            self.addstr('Enter \"change name\" to change your username.\n')
            self.addstr(
                'Enter \"time\" to see your total time spent in Pyarcade.\n')
            self.addstr('Enter \"mailbox\" to access your mailbox.\n\n')

        self.addstr("Enter \"leaderboard\" to see the top scores on "
                    "PyArcade Online.\n")
        self.addstr("Enter 's' to stop the music or 'p' to play it.\n")

    def display(self) -> None:
        """
        :return: None.

        Starts the display and performs I/O operations via the curses module.
        """
        try:
            if not self.testing:
                self.init_window()  # start the window
            if can_play_music and not self.testing:
                self.play_obj = self.wave_obj.play()  # start music
            self.main_loop()

        except Exception as e:
            print(e)
            traceback.print_exc()  # print trace back log of the error

        finally:
            # --- Cleanup on exit ---
            if can_play_music and not self.testing:
                self.play_obj.stop()
            if not self.testing:
                curses.endwin()

    def main_loop(self):
        while True:
            self.display_main_menu()
            self.addstr("> ")
            user_input = self.getstr()

            while self.user_input_is_invalid(user_input):
                self.addstr("Invalid input.\n")
                self.addstr("> ")
                user_input = self.getstr()

            result = self.handle_valid_user_input(user_input)
            if result == 'continue':
                continue
            elif result == 'break':
                break

    def user_input_is_invalid(self, user_input):
        """
        :param user_input: user input to validate.
        :return: bool: whether input is valid.

        Validates user input.
        """
        return user_input != 'quit' and user_input != 'create' \
            and user_input != 'p' and user_input != 's' \
            and (user_input != 'login'
                 if not self.login_manager.is_logged_in()
                 else (user_input != 'logout' and user_input != 'scores'
                       and user_input != 'change name'
                       and user_input != 'delete history'
                       and user_input != 'mailbox')) \
            and user_input != View.CONNECT4 \
            and user_input != View.GOFISH \
            and user_input != View.MASTERMIND \
            and user_input != View.MINESWEEPER \
            and user_input != 'leaderboard' \
            and user_input != 'time'

    def handle_valid_user_input(self, user_input) -> str:
        """
        :return: str: result of running input

        Helper function for parsing/validating user input
        """
        if user_input == 'quit':
            self.login_manager.log_out()
            return "break"
        elif user_input == 'create':
            self.create_account()
        elif user_input == 'login':
            if not self.login_manager.is_logged_in():
                self.login()
            else:
                return "continue"
        elif user_input == 'logout':
            if self.login_manager.is_logged_in():
                self.login_manager.log_out()
            else:
                return "continue"
        elif user_input == 'delete history':
            if self.login_manager.is_logged_in():
                self.delete_log_history()
            else:
                return "continue"
        elif user_input == 'scores':
            self.show_scores()
        elif user_input == 'leaderboard':
            self.show_leaderboard()
        elif user_input == 'time':
            self.show_flight_time()
        elif user_input == 'p':
            if can_play_music and not self.testing:
                self.play_obj = self.wave_obj.play()  # start music
        elif user_input == 's':
            if can_play_music and not self.testing:
                self.play_obj.stop()  # stop music
        elif user_input == 'change name':
            if self.login_manager.is_logged_in():
                self.change_username()
            else:
                return "continue"
        elif user_input == 'mailbox':
            if self.login_manager.is_logged_in():
                self.show_mailbox()
            else:
                return "continue"
        else:
            self.start_game(View.GAME_NUM_TO_NAME[user_input])
        return ""

    def login(self) -> None:
        """
        :return: None.

        Displays the login screen which allows the user to log into their
        account.
        """
        while True:
            self.clear()  # clear window
            self.addstr(self.login_manager.output())
            user_input = self.getstr()
            if user_input == 'quit':  # user wants to quit the login
                if not self.login_manager.is_logged_in():
                    self.login_manager = LoginManager()  # reset login manager
                break
            self.login_manager.input(user_input)

    def create_account(self) -> None:
        """
        :return: None.

        Displays the create account screen which allows the user to make an
        account to store their game history.
        """
        self.clear()
        self.creation_manager.__init__()

        while True:
            self.clear()
            self.addstr(self.creation_manager.output() + '\n')

            user_input = self.getstr()
            self.creation_manager.input(user_input)
            self.addstr(self.creation_manager.output())

            if self.creation_manager.account_created_successfully():
                self.clear()
                self.addstr(self.creation_manager.output())
                break

        self.getstr()

    def change_username(self) -> None:
        """
        :return: None.

        Displays screen for changeing username
        """
        change_message = '********************************\n' \
                         '* Enter your desired username. *\n' \
                         '********************************\n\n' \
                         '> '
        while True:
            self.clear()
            self.addstr(change_message)
            user_input = self.getstr()
            if user_input == "quit":
                break
            elif self.login_manager.change_username(user_input):
                self.clear()
                self.addstr("*********************************************\n"
                            "*        Username has been changed.         *\n"
                            "*  Press enter to return to the Main Menu.  *\n"
                            "*********************************************\n\n")
                self.getstr()
                break
            change_message = '***********************************\n' \
                             '*  That username is not available. *\n' \
                             '* Please enter another one or type *\n' \
                             '*   "quit" to return to the menu.  *\n' \
                             '************************************\n\n' \
                             '> '

    def pause_game(self, testing: bool = False) -> str:
        """
        :param testing: for testing purposes (not for use by client).
        :return string: a string that contains either the quit command or
            relevant game instructions

        This function is implementing the game pause and resume feature.
            Since the feature requires the person to be playing the same
            game and continue from where he left, it keeps game paused till
            resume button is pressed
        """
        self.clear()  # clear window
        pause_message = '****************************\n' \
                        '* The Game has been Paused *\n' \
                        '****************************\n\n' \
                        'Enter "resume" to continue or "quit" to exit ' \
                        'the game.\n' \
                        '> '
        self.addstr(pause_message)
        user_input = self.getstr()

        # Ensure that the player either resumes or quits from this stage
        while user_input != 'resume' and user_input != 'quit':
            self.addstr("Invalid input.\n")
            self.addstr("> ")
            user_input = self.getstr()

        return user_input

    def help_game(self, game_name: str) -> None:
        """
        :param game_name: a string specifying the game that is running.
        :return: None.

        This function displays the help page for the specific game that is running
        """
        self.clear()  # clear window
        if 'Connect4' in game_name:
            game_instructions = 'Connect4 Is pretty simple. Once started, ' \
                                'each player takes turns placing their ' \
                                'token in a slot.\n' \
                                'The tokens are represented by a string ' \
                                'of either ' \
                                '\"X\" or \"Y\",' \
                                '& the slot is represented by the ' \
                                'numbers 0-6.\n\n' \
                                'The winner is the player who connects ' \
                                'four of his ' \
                                'tokens in a line, ' \
                                'either horizontally, vertically, or ' \
                                'diagonally.\n' \
                                'Scores are based on the number of times a ' \
                                'player wins a match\n\n' \
                                'Instructions: Enter a move as player ' \
                                'X or Y with \"X,0\" ' \
                                'to drop X in the first column ' \
                                '(without the quotation marks).\n'

        if 'Mastermind' in game_name:
            game_instructions = 'The goal of this game is for the ' \
                                'player to correctly guess a hidden ' \
                                'sequence of ' \
                                '4, 5, or 6 numbers depending upon ' \
                                'game difficulty.\n\n' \
                                'The only hints the player will receive ' \
                                'during the ' \
                                'game is how many of the numbers they ' \
                                'guessed are in ' \
                                'the right location (bulls)\n' \
                                'and how many are located somewhere else ' \
                                'in the ' \
                                'sequence (cows).\n' \
                                'Scores are updated based on a correct ' \
                                'guess in the correct position' + '\n\n' + \
                                'Instructions: User input should be in ' \
                                'the form of: \"####\", ' \
                                'where # is a digit ' \
                                '(without the quotation marks).\n'

        if 'Minesweeper' in game_name:
            game_instructions = 'The goal of this game is for ' \
                                'the player to uncover all of the ' \
                                'cells of a hidden grid ' \
                                'that do not contain a mine.\n' \
                                'As the player uncovers cells, all ' \
                                'of the adjacent cells ' \
                                'that do not contain mines will be ' \
                                'revealed;\n' \
                                'and the number of mines near certain ' \
                                'cells ' \
                                'will appear.\n\n' \
                                'Score is based on number of ' \
                                'matches won.\n\n' \
                                'Instructions: Enter square position as: ' \
                                '\"1,1\"\nTo toggle a ' \
                                'squares flag add f or F as: ' \
                                '\"1,1,f\".\n' \
                                '(without the quotation marks).\n'

        if 'Go Fish' in game_name:
            game_instructions = 'The goal of the game is to collect ' \
                                'a pair of 4 cards having the same number. ' \
                                'The game is played between 2 players.\n' \
                                'All scores are based on the number of ' \
                                'correct guesses' \
                                ' the user/computer makes' + '\n\n' \
                                                             'PLEASE NOTE:\n(1) Once you play, the ' \
                                                             'computer will ' \
                                                             'automatically play its turn and you will ' \
                                                             'be prompted to play again once the ' \
                                                             'computers ' \
                                                             'turn is over.\n(2) The player who ' \
                                                             'guesses correct when its their turn ' \
                                                             'continues to play ' \
                                                             'until they make an incorrect guess\n' \
                                                             '(3) Any pairs ' \
                                                             '(e.g 10 of Hearts, 10 of Diamonds) ' \
                                                             'present in players ' \
                                                             'hand will be removed and ' \
                                                             'added to score after current turn''\n\n' + \
                                'Instructions: User input should be: ' \
                                '\"2-10\" or \"J-A\" ' \
                                '(without the quotation marks).\n'

        help_message = '****************************\n' \
                       '*        Help Screen      *\n' \
                       '****************************\n\n' \
                       + game_instructions + \
                       '\nEnter "exit" to return to game.\n' \
                       '> '
        self.addstr(help_message)
        user_input = self.getstr()

        # Ensure that the player either resumes or quits from this stage
        while user_input.lower() != 'exit':
            self.addstr("Invalid input.\n")
            self.addstr("> ")
            user_input = self.getstr()

    def start_game(self, game_name: str) -> None:
        """
        :param game_name: a string containing the game to be started.
        :return: None.

        Displays the chosen game using the game manager.
        """
        self.game_manager.set_state(game_name)
        instruct_message = 'Enter \"pause\" to take a break.\n' \
                           'Enter \"help\" to check the Help Section. \n' \
                           'Enter \"save and quit\" to save and return to ' \
                           'the main menu or just \"quit\" to return without ' \
                           'saving.\n' \
                           'To (switch) to a certain game enter its name ' \
                           '(Case_Sensitive).\n' \
                           '> '

        while True:
            self.clear()  # clear window
            self.addstr(self.game_manager.output() + '\n')
            self.addstr(instruct_message)
            user_input = self.getstr()

            if user_input == 'pause':  # Checking if the user paused the game
                user_input = self.pause_game()
            if user_input.lower() == 'help':  # Checking if the user want to check Help section
                self.help_game(game_name)
            if 'quit' in user_input:
                if self.login_manager.is_logged_in():  # save score if login
                    self.game_manager.save_score_to_db(
                        self.login_manager.get_curr_user(), self.login_manager.get_curr_pswd())
                if user_input == 'quit':  # wants to quit without saving state
                    self.game_manager.reset_state()
                break

            self.game_manager.input(user_input)

    def show_scores(self) -> None:
        """
        :return: None.

        Displays a user's scores given that their logged in.
        """
        while True:
            self.clear()  # clear window
            self.addstr("**************************************\n")
            self.addstr("**              SCORES              **\n")
            self.addstr("**************************************\n")
            self.addstr(Database().get_user_scores(
                self.login_manager.get_curr_user(),
                self.login_manager.get_curr_pswd()))
            self.addstr("\nEnter \"quit\" to return to the main menu.\n> ")
            user_input = self.getstr()
            if user_input == 'quit':  # user wants to quit the login
                break

    def show_leaderboard(self) -> None:
        """
        :return: None.

        Displays PyArcade's leaderboard.
        """
        while True:
            self.clear()  # clear window
            self.addstr("**************************************\n")
            self.addstr("**           LEADERBOARD            **\n")
            self.addstr("**************************************\n")
            self.addstr(Database().get_leaderboard())
            self.addstr("\nEnter \"quit\" to return to the main menu.\n> ")
            user_input = self.getstr()
            if user_input == 'quit':  # user wants to exit the leaderboards
                break

    def show_flight_time(self):
        """
        :return: None.

        Displays PyArcade's leaderboard.
        """
        flight_hours = Database().get_user_flight_hours(self.login_manager.get_curr_user(),
                                                        self.login_manager.get_curr_pswd())
        total_minutes = 0
        for idx in range(0, len(flight_hours)):
            current_time = flight_hours[idx]['duration'].split(':')
            total_minutes += ((int(current_time[0]) * 3600) + (int(current_time[1]) * 60)
                              + int(current_time[2].split('.')[0]))
        hours = total_minutes // 3600
        minutes = (total_minutes % 3600) // 60
        seconds = total_minutes - ((hours * 3600) + (minutes * 60))
        while True:
            self.clear()  # clear window
            self.addstr("**************************************\n")
            self.addstr("**   TOTAL TIME SPENT IN PYARCADE   **\n")
            self.addstr("**************************************\n")
            self.addstr("You've spent " + str(hours) + " hour(s), " + str(minutes) + " minute(s) and "
                        + str(seconds) + " second(s) in Pyarcade.\n")
            self.addstr("\nEnter \"quit\" to return to the main menu.\n> ")
            user_input = self.getstr()
            if user_input == 'quit':  # user wants to exit their time played
                break

    def delete_log_history(self) -> None:
        """
        :return: None.

        Displays a user's scores given that their logged in.
        """
        while True:
            self.clear()  # clear window
            Database().del_user_logs(self.login_manager.get_curr_user(),
                                     self.login_manager.get_curr_pswd())
            self.addstr("**************************************\n")
            self.addstr("**        User History Deleted      **\n")
            self.addstr("*    Please enter 'quit' to return   *\n")
            self.addstr('*            to the menu.            *\n')
            self.addstr("**************************************\n")
            self.addstr("\nEnter \"quit\" to return to the main menu.\n> ")
            user_input = self.getstr()
            if user_input == 'quit':  # user wants to quit the login
                break

    def show_mailbox(self) -> None:
        """
        :return: None.

        Implements the Mailbox Feature of Pyarcade Online
        """
        inbox_manager = InboxManager()

        inbox_manager.update_default_data(self.login_manager.get_curr_user(),
                                          self.login_manager.get_curr_pswd())
        while True:
            self.clear()  # clear window
            heading = '***********************************\n' \
                      '****          Mailbox          ****\n' \
                      '****   Please enter "quit" to  ****\n' \
                      '****     return to last menu   ****\n' \
                      '***********************************\n\n'
            self.addstr(heading)
            self.addstr(inbox_manager.output())
            user_input = self.getstr()

            while True:  # Checking input validity
                error, index = inbox_manager.check_input_invalidity(user_input)
                if index:
                    break
                elif not index:
                    self.addstr(error)
                    user_input = self.getstr()
            if user_input == 'quit':  # user wants to quit the mailbox
                break

            inbox_manager.input(user_input)
