from pyarcade.database import Database
from pyarcade.state import State


class AccountManager(State):
    """
    A class that enables a user to create an account to store game history.
    """

    def __init__(self):
        """
        Initialize the state of account creation. Provides the user with instructions on how to create
            an account and initializes all of the variables to their starting values.
        """
        self.message = "***********************************\n" \
                       "****     CREATE AN ACCOUNT     ****\n" \
                       "***********************************\n" \
                       "***********************************\n" \
                       "**** Enter a username and then ****\n" \
                       "****     enter a password.     ****\n" \
                       "***********************************\n\n"
        self.usernameAccepted = False
        self.passwordAccepted = False
        self.usernameTaken = False
        self.currentUsername = ""
        self.database = Database()

    def create_account(self, username, password):
        """
        :param username: The user's selected username.
        :param password: The user's desired password.
        :return: None.

        Calls the database and adds the current user to the system.
        """
        self.database.add_user(username, password)

    def validate_username(self, username):
        """
        :param username: The user's desired username to check for existence.
        :return: True if username is available, False if it's already in use.

        Verifies whether the username already exists. If so, the user must select a different one.
        """
        return self.database.is_username_available(username)

    def account_created_successfully(self):
        """
        :return: Boolean: Represents whether or not the account was created.

        Function that determines whether or not the credentials were accepted.
        """
        return self.passwordAccepted

    def input(self, text_input):
        """
        :param text_input: Input from the user - either a username or password
        :return: None.

        Function that takes user input and parses it according to the user's current progress in creating
        an account.
        """
        if self.usernameAccepted:
            self.create_account(self.currentUsername, text_input)
            self.passwordAccepted = True
        else:
            if self.validate_username(text_input):
                self.usernameAccepted = True
                self.currentUsername = text_input
            else:
                self.usernameTaken = True

    def output(self):
        """
        :return: return_message: A string representing the current state of creating a user account.

        This function determines which step of the account creation process the user is in and returns
        a string that accomodates for that.
        """
        return_message = self.message
        if self.usernameAccepted:
            if self.passwordAccepted:
                return_message = "***********************************************\n" \
                                  "****     Account created successfully!     ****\n" \
                                  "****  Press Enter to return to Main Menu.  ****\n" \
                                  "***********************************************\n\n" \
                                  "> "
            else:
                return_message += "Password: "
        else:
            if self.usernameTaken:
                return_message += "Sorry, that username is taken. Please enter another.\n"
            return_message += "Username: "

        return return_message
