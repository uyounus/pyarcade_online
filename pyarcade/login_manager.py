from pyarcade.database import Database
from pyarcade.state import State
from datetime import datetime


class LoginManager(State):
    """
    A class representing a state (screen) for logging into PyArcade Online.
        It is also responsible for other log-in related tasks such as keeping
        track of how long a user was logged in and changing username.
    """
    current_user = ""
    current_password = ""

    def __init__(self):
        self.heading = "***********************************\n" \
                       "****          LOG IN           ****\n" \
                       "***********************************\n"
        self.status = "Please enter your username.\n"
        self.entering_username = True
        self.temp_username = ""
        LoginManager.current_user = ""
        LoginManager.current_password = ""
        self.logged_in = False
        self.datetimeLoggedIn = None
        self.database = Database()

    def log_out(self) -> None:
        """
        :return: None.

        Documents the time spent on PyArcade and logs the user out.
        """
        # Document the time spent on PyArcade
        self.database.add_user_flight_hours(LoginManager.current_user,
                                            LoginManager.current_password,
                                            self.duration_logged_in())
        self.__init__()

    def duration_logged_in(self) -> str:
        """
        :return: str: description of how long user was logged in, or "" if N/A.
        """
        if self.datetimeLoggedIn is None:
            return ""
        return str(datetime.now() - self.datetimeLoggedIn)

    def is_logged_in(self) -> bool:
        """
        :return: bool: whether a user is logged in.

        Check whether a user is logged in.
        """
        return self.logged_in

    @staticmethod
    def get_curr_user() -> str:
        """
        :return: str: username of current user.

        Get the username of the current user logged in.
        """
        return LoginManager.current_user

    @staticmethod
    def get_curr_pswd() -> str:
        """
        :return: str: password of current user.

        Get the password of the current user logged in.
        """
        return LoginManager.current_password

    def change_username(self, new_username: str) -> bool:
        """
        :return: bool: whether the change was successful.

        Change the current user's username.
        """
        if self.database.is_username_available(new_username):
            self.database.change_username(self.get_curr_user(), self.get_curr_pswd(), new_username)
            LoginManager.current_user = new_username
            return True
        return False

    def input(self, text_input: str) -> None:
        """
        :returns: None.

        Take user input.
        """
        if self.logged_in:
            return
        elif self.entering_username:
            if not self.database.is_username_available(text_input):
                self.entering_username = False
                self.temp_username = text_input
                self.status = 'Hello ' + text_input + ', please enter your ' \
                                                      'password.\n'
            else:
                self.status = 'The username does not exist, try again.\n'
        else:
            if self.database.is_valid_username_password(self.temp_username,
                                                        text_input):
                LoginManager.current_password = text_input
                self.perform_login()
            else:
                self.status = 'Incorrect password, try again.\n'

    def output(self) -> str:
        """
        :returns: str: output to display.

        Get output for the user.
        """
        return self.heading + self.status + 'Enter "quit" to return to the main menu.\n> '

    def perform_login(self) -> None:
        """
        :return: None.

        Logs the user in.
        """
        self.datetimeLoggedIn = datetime.now()
        self.logged_in = True
        LoginManager.current_user = self.temp_username
        self.status = 'You have logged in successfully.\n'
