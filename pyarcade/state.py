from abc import ABC, abstractmethod


class State(ABC):
    """
    An abstract class to be used as an *interface* that *screens* (that are
        not necessarily games, e.g. create account) in PyArcade must implement so
        that the View class could use them as states.
    """

    @abstractmethod
    def input(self, text_input) -> None:
        """
        :param text_input: an input string.
        :return: None.

        Abstract method representing a state's input method.
        """
        raise NotImplementedError

    @abstractmethod
    def output(self) -> str:
        """
        :return: str: the output to display.

        Abstract method representing a state's output method.
        """
        raise NotImplementedError
