from pyarcade.connect4.connect4 import Connect4
from pyarcade.go_fish.go_fish import GoFish
from pyarcade.mastermind.mastermind import Mastermind
from pyarcade.minesweeper.minesweeper import Minesweeper
from pyarcade.database import Database
from pyarcade.game_state import GameState
from typing import Optional, List, TypeVar


class GameManager(GameState):
    """
    A class employing all games in PyArcade according to the "State" design pattern.
    """

    def __init__(self):
        """
        Initializes a collection of the games in PyArcade.
        """
        self.game_dictionary = {
            "Connect4": Connect4(),
            "Go Fish": GoFish(),
            "Mastermind": Mastermind(),
            "Minesweeper": Minesweeper()
        }
        self.state = None
        self.state_name = ""
        self.database = Database()

    def set_state(self, state: str) -> None:
        """
        :param state: str: the name of the game to be played.
        :return: None.

        Changes the state (the current game).
        """
        self.state_name = state
        self.state = self.game_dictionary[state]

    def input(self, command) -> None:
        """
        :param command: the user's input.
        :return: None.

        Takes in user input.
        """
        if command in self.game_list():
            self.set_state(command)
        else:
            self.state.input(command)

    def output(self) -> str:
        """
        :return: string: output of the current game being played.
        """
        return self.state.output()

    def save_score_to_db(self, current_user: str, current_pswd: str) -> bool:
        """
        :param current_user: name of current user.
        :param current_pswd: password of current user.
        :return: bool: whether or not save was successful.

        Method representing a game-state's save_score_to_db method which
            saves a score to the database.
        """
        return self.database.save_score_to_db(current_user, current_pswd, self.state_name,
                                              self.get_score())

    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        return self.state.get_score()

    def game_list(self) -> List[str]:
        """
        :return: list (of strings) of all game names that have been added
        """
        return [str(name) for name in self.game_dictionary.keys()]

    def reset_game_list(self) -> None:
        """
        :return: None.

        Resets the game list
        """
        self.game_dictionary = {}

    def reset_state(self) -> None:
        """
        :return: None.

        Resets the current state.
        """
        temp_dictionary = {"Connect4": Connect4(),
                           "Go Fish": GoFish(),
                           "Mastermind": Mastermind(),
                           "Minesweeper": Minesweeper()}
        for game_name in self.game_dictionary.keys():
            if game_name == self.state_name:
                self.game_dictionary[game_name] = temp_dictionary[game_name]
