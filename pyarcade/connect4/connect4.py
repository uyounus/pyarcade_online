from pyarcade.input_system import *
from pyarcade.game_state import GameState


class Connect4(GameState):
    """
    A class representing a single instance of a Connect4 game session.
    """

    def __init__(self, difficulty: str = 'easy'):
        """
        :param difficulty: str: 'easy', 'medium' or 'hard'
        Initializes a game session of Connect4.
        """

        self.game_storage = []
        self.board = self._get_board_of_shape(7, 6)
        self.difficulty = difficulty
        self.state = "GAME IN SESSION"
        if self.difficulty == "hard":
            self.board = self._get_board_of_shape(10, 7)
        elif self.difficulty == "medium":
            self.board = self._get_board_of_shape(8, 7)

        self.prompt = "**************************\n" \
                      "*  Welcome to Connect4!  *\n" \
                      "**************************\n"
        self.prompt_bool = True
        self.x_score = 0
        self.y_score = 0

    def _get_board_of_shape(self, cols: int = 7, rows: int = 6) -> list:
        """
        :param cols: int: number of rows.
        :param rows: int: number of cols.
        :return: list: the resulting 2D list.

        Create a board of the given shape.
        """
        return [['_' for _ in range(cols)] for _ in range(rows)]

    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        return self.x_score

    def get_board(self) -> [[]]:
        """
        :return board: list: The board displayed as a 2D list.

        Returns the current game board.
        """
        return self.board

    def input(self, command) -> None:
        """
        :param command: str: text input provided by the user.
        :return: None.

        Processes the provided user input.
        """
        move = validate_connect4_move(command, self.difficulty)
        if move == "reset":
            self.reset()
        elif move == "clear":
            self.clear()
        elif move == "hard" or move == "medium" or move == "easy":
            self.__init__(move)
        elif move is not None:
            self.move(move[0], move[1])
            if self.evaluate_game(move[0]):
                self.prompt = "Hooray! Player " + str(
                    move[0]) + " won the game!\n"
                self.prompt_bool = True
                if move[0] == "X":
                    self.x_score += 1
                elif move[0] == "Y":
                    self.y_score += 1
                self.reset()

    def output(self) -> str:
        """
        :return: string_output: str: Concatenation of all the game
            session's data.

        Outputs a string representation of the current game board.
        """
        string_output = ""
        if self.prompt_bool:
            string_output += self.prompt
            self.prompt_bool = False

        for x in range(0, len(self.board[0])):
            string_output += " " + str(x) + " "
        string_output += ('\n---' + ('---' * (len(self.board[0]) - 1)) + "\n")

        for row in range(len(self.board)):
            for col in range(len(self.board[0])):
                string_output += (' %s ' % self.board[row][col])
            string_output += "\n"
        string_output += ('---' + ('---' * (len(self.board[0]) - 1))) + "\n"

        string_output += \
            "Player X Score: " + str(self.x_score) + "\n" \
            "Player Y Score: " + str(self.y_score) + "\n\n" \
            "Difficulty: " + self.difficulty + "\n" \
            "To change the difficulty and reset the game, enter " \
            "\"easy\", \"medium\", or \"hard\"" + "\n" \
            "\nEnter \"reset\" to reset the current session," \
            " or \"clear\" to clear Connect4\'s game history."

        return string_output

    def start(self) -> str:
        """
        :return: output: function: A string representing the current game 
            session's state.

        Display the current state of the game upon start.
        """
        return self.output()

    def clear(self) -> None:
        """
        :return: None.

        Resets the history of the current and past game sessions.
        """
        self.game_storage = []

    def store_game(self) -> None:
        """
        :return: None.

        Saves the current game board to game storage.
        """
        self.game_storage.append(self.board)

    def reset(self) -> None:
        """
        :return: None.

        Sets the current game board back to it's initial state.
        """
        if self.difficulty == 'hard':
            self.board = self._get_board_of_shape(10, 7)
        elif self.difficulty == 'medium':
            self.board = self._get_board_of_shape(8, 7)
        else:
            self.board = self._get_board_of_shape(7, 6)

    def evaluate_game(self, player) -> bool:
        """
        :param player: char: Character representing the current player's 
            game piece.
        :return: boolean: Represents whether or not the current player has 
            won.

        Goes through all possible winning combinations for the current
            player to determine whether or not they have placed four game
            pieces in a row.
        """
        for row in range(len(self.board)):
            for column in range(len(self.board[0])):
                if (self.check_4_left_diagonal(row, column, player) or
                        self.check_4_right_diagonal(row, column, player) or
                        self.check_4_right(row, column, player) or
                        self.check_4_down(row, column, player)):
                    self.store_game()
                    return True
        return False

    def check_4_left_diagonal(self, row, column, player) -> bool:
        """
        :param row: int: current row postion to check left diagonal from.
        :param column: int: current row postion to check left diagonal from.
        :param: player: char: The current player's game character (X or Y).
        :return: boolean: Indicating whether or not there was four characters
            in a row from the current row, column.

        Checks if the current player has a winning connection in the left
            diagonal direction from the current column and row.
        """
        if self.board[row][column] != player:
            return False
        elif row - 3 < 0:
            return False
        elif column + 3 >= len(self.board[0]):
            return False
        elif (self.board[row - 1][column + 1] == player and
              self.board[row - 2][column + 2] == player and
              self.board[row - 3][column + 3] == player):
            return True

    def check_4_right_diagonal(self, row, column, player) -> bool:
        """
        :param row: int: current row postion to check right diagonal from.
        :param column: int: current row postion to check right diagonal from.
        :param: player: char: The current player's game character (X or Y).
        :return: boolean: Indicating whether or not there was four
            characters in a row from the current row, column.

        Checks if the current player has a winning connection in the right
             diagonal direction from the current column and row.
        """
        if self.board[row][column] != player:
            return False
        elif row + 3 >= len(self.board):
            return False
        elif column + 3 >= len(self.board[0]):
            return False
        elif (self.board[row + 1][column + 1] == player and
              self.board[row + 2][column + 2] == player and
              self.board[row + 3][column + 3] == player):
            return True

    def check_4_right(self, row, column, player) -> bool:
        """
        :param row: int: current row postion to check to the right of.
        :param column: int: current row postion to check to the right of.
        :param: player: char: The current player's game character (X or Y).

        :return: boolean: Indicating whether or not there was four
            characters in a row from the current row, column.

        Checks if the current player has a winning connection in the
            right direction from the current column and row.
        """
        if self.board[row][column] != player:
            return False
        elif row >= len(self.board[0]):
            return False
        elif column + 3 >= len(self.board[0]):
            return False
        elif (self.board[row][column + 1] == player and
              self.board[row][column + 2] == player and
              self.board[row][column + 3] == player):
            return True

    def check_4_down(self, row, column, player) -> bool:
        """
        :param row: int: current row postion to check down from.
        :param column: int: current row postion to check down from.
        :param: player: char: The current player's game character (X or Y).
        :return: boolean: Indicating whether or not there was four
            characters in a row from the current row, column.

        Checks if the current player has a winning connection in the
             down direction from the current column and row.
        """
        if self.board[row][column] != player:
            return False
        elif row + 3 > len(self.board) - 1:
            return False
        elif (self.board[row + 1][column] == player and
              self.board[row + 2][column] == player and
              self.board[row + 3][column] == player):
            return True

    def move(self, player, slot: int) -> None:
        """
        :param: player: char: The current player's game character
            (X or Y).
        :param: slot: int: The column to make the move on specified
            by the user.
        :return: None.

        Makes the move selected by the user on the current game board.
        """
        row = len(self.board) - 1
        while row >= 0:
            if self.board[row][slot] == "_":
                self.board[row][slot] = player
                break
            row -= 1

    def print_board(self) -> None:
        """
        :return: None.

        Prints the board of the current game session.
        """
        for row in self.board:
            print(row)
