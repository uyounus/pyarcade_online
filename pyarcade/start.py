from pyarcade.view import View


def run_pyarcade():
    """
    This will effectively become our program entry-point.
        Displays PyAracde to the user via the view module.
    """
    view = View()
    view.display()


if __name__ == "__main__":
    run_pyarcade()
