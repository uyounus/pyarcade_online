from typing import Optional, List
import random
from pyarcade.input_system import *
from pyarcade.game_state import GameState


class Mastermind(GameState):
    """
    A class representing a single(!) Mastermind game session
    """
    all_histories = []  # a class variable to store histories of all games

    def __init__(self, difficulty: str = "easy"):
        """
        Initializes a mastermind game with a new hidden sequence and an empty
        history of guesses.
        """
        self.history = []  # history of guesses for current game/session
        # string representing last evaluation
        self.evaluation = ""
        self.game_state = "GAME IN SESSION"
        self.score = 0
        self.seq_size = 4
        self.correct_guesses = []
        self.difficulty = difficulty
        if difficulty == "medium":
            self.seq_size = 5
        elif difficulty == "hard":
            self.seq_size = 6
        self.hidden_sequence = self.generate_hidden_sequence()

    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        return self.score

    def generate_hidden_sequence(self) -> List[int]:
        """
        :return: hidden_sequence List[int]: A sequence of 4 digits to be
            guessed by the player.
        """
        return [random.randint(0, 9) for _ in range(self.seq_size)]

    def update_hidden_sequence(self) -> None:
        """
        :return: None.

        Updates the object's hidden sequence with a new one.
        """
        self.hidden_sequence = self.generate_hidden_sequence()

    def eval_guess(self, guess: List[int]) -> str:
        """
        :param guess: List[int]: a (validated) guess provided as a list of
            ints.
        :return: my_eval str: the evaluation for the provided guess.

        Evaluates the given (validated) guess.
            Definition of evaluation:
                This includes for each digit in the guessed sequence,
                whether that digit is:
                '✘' means the digit is nowhere in the hidden sequence.
                '≈' means the digit is somewhere else in the hidden sequence.
                '✔' means the digit is exactly right.
        """
        if self.game_state == "GAME WON" or self.game_state == "GAME OVER":
            return ""

        my_eval = "Evaluation:\n\n"
        exact_match = True  # stays True as long as guess is an exact match
        evaluated_guess = [''] * self.seq_size

        for idx in range(len(guess)):  # traverse guess to evaluate it
            current_digit = guess[idx]
            if current_digit not in self.hidden_sequence:
                exact_match = False
                evaluated_guess[idx] = "{}-✘".format(str(current_digit))
            elif current_digit != self.hidden_sequence[idx]:
                exact_match = False
                evaluated_guess[idx] = "{}-≈".format(str(current_digit))
            else:
                if idx not in self.correct_guesses:
                    self.correct_guesses.append(idx)
                    self.score += 1
                evaluated_guess[idx] = "{}-✔".format(str(current_digit))

        # append to history of guesses
        self.history.append(evaluated_guess)

        for idx in range(self.seq_size):  # append all guesses to output string
            my_eval += '\t'.join([guess[idx] for guess in self.history])
            my_eval += '\n'

        if exact_match:  # store the entire history if exactly matches
            Mastermind.all_histories.append(self.history)
            self.game_state = "GAME WON"
        elif len(self.history) == 12:  # if exhausted guesses
            Mastermind.all_histories.append(self.history)
            self.game_state = "GAME OVER"

        return my_eval

    def input(self, text_input: str) -> None:
        """
        :param text_input: str: text input provided by the user.
        :return: None.

        Processes the provided user input.
        """
        if text_input == 'reset':  # "reset the current game/session"
            self.__init__()
        elif text_input == 'clear':  # "clear the game history of Mastermind"
            Mastermind.all_histories = []
        elif text_input == "easy" or text_input == "medium" or text_input == "hard":
            self.__init__(text_input)
        else:  # input will be ignored if it is invalid
            guess = validate_mastermind_guess(text_input, self.difficulty)
            # guess will have 4 digits if input is valid
            if len(guess) == self.seq_size:
                self.evaluation = self.eval_guess(guess)

    def output(self) -> str:
        """
        :return: str: the output to display.

        Provide output to the user (reflects the game state).
        """
        welcome_string = "**************************\n" \
                         "* Welcome to Mastermind! *\n" \
                         "**************************\n"
        if self.difficulty == "easy":
            prompt = "\"1234\""
        elif self.difficulty == "medium":
            prompt = "\"12345\""
        elif self.difficulty == "hard":
            prompt = "\"123456\""

        instruct = "Enter digits to guess the hidden sequence (e.g. " \
                   + prompt + ")).\n"
        instruct += "'✘' means bad digit, '≈' means incorrect " \
                    "position, '✔' means correct guess.\n"

        return ("" if self.history != [] else welcome_string) + \
               self.game_state + '\n' + \
               self.evaluation + '\n' + \
               "Current Score: " + str(self.score) + '\n' + \
               "Difficulty: " + self.difficulty + \
               "\nTo change the difficulty and reset the game," \
               " enter \"easy\", \"medium\", or \"hard\"" + "\n\n" + \
               instruct + \
               '\nEnter "reset" to reset the current session, ' + \
               'or "clear" to clear Mastermind\'s game history.'
