from pyarcade.database import Database
from pyarcade.state import State


class InboxManager(State):
    """
    This class is representing a state (Screen) for Inbox feature of Pyarcade
        Online. It is responsible for sending, receiving and reviewing messages.
    """

    def __init__(self, testing: bool = False, test_input: list = []):
        """
        :param testing: bool: whether currently testing.
        :param test_input: list: the list of string inputs to read from

        Initializes the InboxManager with A State Class and
            default user details
        """
        self.intro = 'Enter \"send\" to send a new message\n' \
                     'Enter \"inbox\" to view all received messages.\n' \
                     'Enter \"outbox\" to view all sent messages\n' \
                     'Enter \"delete\" to revert sent messages' \
                     ' and clear outbox\n\n' \
                     '> '
        self.message = ""
        self.status_pin = 0
        self.recipient = ""
        self.current_user = ""
        self.current_pswd = ""
        self.error_message = "Invalid Input.\n> "
        self.users = ""
        self.database = Database()

    def input(self, user_input: str) -> None:
        """
        :returns: None.

        Take user input.
        """
        if user_input == 'send':
            self.send_instructions()
        elif user_input == 'inbox':
            self.show_inbox()
        elif user_input == 'outbox':
            self.show_outbox()
        elif user_input == 'delete':
            self.delete_sent_items()
        else:
            self.send_message(user_input)

    def send_instructions(self):
        """
        :return: None.

        Update the instructions to send messages
        """
        self.status_pin = 1
        all_users = self.database.get_all_usernames()
        self.users = ','.join(all_users)
        self.message = 'List of all users in Pyarcade is below:\n' \
                       + self.users + \
                       '\n\nTo send a message to a username, ' \
                       'you need 3 items:' \
                       '\n 1) username\n 2) title of message\n ' \
                       '3) message to be sent\n\n' \
                       'Format to send message is below ' \
                       '(without quotation marks):\n' \
                       '\"Username_of_recipient:Title_of_message:' \
                       'Actual_Message_to_Send\"\n\n' \
                       'Type here > '

    def send_message(self, to_send: str):
        """
        :param to_send: string that contains username, title and message
        :return: None.

        Will send the requested message by updating the database
        """

        elements = to_send.split(':')
        self.recipient = elements[0]
        title = elements[1]
        message = elements[2]
        self.database.send_message(self.current_user, self.current_pswd,
                                   self.recipient, title, message)

        self.message = 'Message Sent.\n\n'
        self.status_pin = 0

    def delete_sent_items(self) -> None:
        """
        :return: None.

        Deletes all the sent messages in outbox for the current user
            and also revert messages from inbox of recipient
        """
        check = self.database.del_sent_messages(self.current_user,
                                                self.current_pswd)
        self.status_pin = 0
        if check:
            self.message = 'All Sent Messages Reverted and ' \
                           'Deleted from Outbox.\n\n'
        else:
            self.message = "Couldn't delete all messages. Try again Later.\n\n"

    def show_inbox(self) -> None:
        """
        :return: None.

        Prints out all the messages of current user in the inbox
        """
        self.status_pin = 2
        all_messages = self.database.get_inbox_messages(self.current_user,
                                                        self.current_pswd)
        all_messages_str = str(all_messages)
        elements = all_messages_str.split(',')
        printable_messages = ''
        for items in elements:
            printable_messages += items + '\n'

        self.message = 'All Messages In Inbox Are As Below:\n' \
                       + printable_messages + '\nEnter \"quit\" to return ' \
                                              'to menu\n> '

    def show_outbox(self) -> None:
        """
        :return: None.

        Prints out all the sent messages present in inbox of user
        """
        self.status_pin = 2
        all_messages = self.database.get_outbox_messages(self.current_user,
                                                         self.current_pswd)
        all_messages_str = str(all_messages)
        elements = all_messages_str.split(',')
        printable_messages = ''
        for items in elements:
            printable_messages += items + '\n'

        self.message = 'All Sent Messages Are As Per Below:\n' \
                       + printable_messages + '\nEnter \"quit\" to ' \
                                              'return to menu\n> '

    def output(self) -> str:
        """
        :returns: str: output to display.

        Get output for the user.
        """
        if self.status_pin == 0:
            return self.message + self.intro
        elif self.status_pin == 1 or self.status_pin == 2:
            return self.message

    def check_input_invalidity(self, user_input: str):
        """
        :param user_input: user input to validate
        :return: a tuple: 1) Error Message: to show the type of error
            2) Bool: to get another input or not

        Check if the input is invalid to ask again for input
        """
        if self.status_pin == 0 or self.status_pin == 2:
            if user_input != 'send' and user_input != 'inbox' \
                    and user_input != 'outbox' and user_input != 'delete' \
                    and user_input != 'quit':
                return self.error_message, False
            else:
                return '', True

        elif self.status_pin == 1:
            error = '> Invalid Input.\n> '
            check_format = self.check_send_format(user_input)
            check_name = self.check_send_username(user_input)

            if not check_name:
                error = error + 'Wrong Username. Please check\n> '
            if not check_format:
                error = error + 'Wrong Message Format. Please check\n> '
            if user_input == 'quit' or (check_format and check_name):
                return '', True
            else:
                return error, False

    def check_send_username(self, user_input: str) -> bool:
        """
        :param user_input: string that contains username, title and message
        :return: Bool: True if it exists

        Check if the username to send message to does exist
        """

        elements = user_input.split(':')  # split the string by colon :
        username = elements[0]

        # check username exists. Returns True if not present
        check = self.database.is_username_available(username)
        return not check

    @staticmethod
    def check_send_format(user_input: str) -> bool:
        """
        :param user_input: string that contains username, title and message
        :return: Bool: True if the format is correct

        Checks if the send message is in correct format with 2 semicolons
        """
        counter = user_input.count(':')  # count if the format is correct
        if counter == 2:
            return True
        else:
            return False

    def update_default_data(self, username: str, password: str) -> None:
        """
        :param username: the user_name of currently logged in user
        :param password: the password of currently logged in user
        :return: None.

        Update the username and password with default login credentials
        """
        self.current_user = username
        self.current_pswd = password
