from abc import abstractmethod
from pyarcade.state import State


class GameState(State):
    """
    An abstract class to be used as an *interface* that all games in PyArcade
        must implement so that game manager could use them as states.
    """

    @abstractmethod
    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        raise NotImplementedError
