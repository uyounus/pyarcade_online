from random import shuffle
from random import choice
from pyarcade.game_state import GameState
from pyarcade.go_fish import VALUES, SUITS
from pyarcade.input_system import validate_go_fish_guess


def hand_contains_value(value: str, hand: [[str, str]]) -> bool:
    """
     :param value: str: The value of the card that needs to be found.
     :param hand: [[]]: The set of cards that needs to be searched.
     :return: bool, true if hand contains value and false otherwise.

     A static Method for checking
    """
    for card in hand:
        if card[0] == value:
            return True
    return False


class GoFish(GameState):
    """
    A class representing a single instance of a Go Fish game session.
    """
    all_histories = []  # Variable for keeping track of overall game history

    def __init__(self):
        """
        Initializes a game session of Go Fish.
        """
        self.wins = 0
        self.total = 0
        self.deck = []
        self.player_hand = []
        self.computer_hand = []
        self.computer_pairs = []
        self.player_pairs = []
        self.history = []
        self.evaluation = ""
        self.res = ""
        self.pair_string = ""
        self.player_score = 0
        self.computer_score = 0
        self.comp_result = ""
        self.game_state = 'GAME IN SESSION'

        for val in VALUES:
            for suit in SUITS:
                self.deck.append((val, suit))

        shuffle(self.deck)
        self.deal()

    def get_score(self) -> int:
        """
        :return: int: the current score of the game.
        """
        return self.player_score

    def input(self, text_input: str) -> None:
        """
        :param text_input: str: text input provided by the user.
        :return: None.

        Processes the provided user input.
        """
        if text_input == 'reset':  # "reset the current game/session"
            self.__init__()
            self.evaluation = ""
        elif text_input == 'clear':  # "clear the game history of GoFish"
            GoFish.history = []
            self.history = []
            self.evaluation = "Go Fish Cleared"
        elif self.game_state != 'GAME IN SESSION':
            None
        elif self.check_game_over():
            self.evaluation = self.check_game()
            self.game_state = self.check_game()
        else:  # input will be ignored if it is invalid
            guess = validate_go_fish_guess(text_input)

            if len(guess) == 1 or guess == "10" and guess != "":
                # guess will have 1 digit if input is valid
                self.evaluation = self.player_guess(guess) + self.pair_string

    def output(self) -> str:
        """
        :return: str: the output to display.

        Provide output to the user (reflects the game state).
        """
        if self.comp_result == "":
            self.comp_result = "Still your turn, computer has " \
                               "not played yet\n"
        welcome_string = "***********************\n" \
                         "* Welcome to Go Fish! *\n" \
                         "***********************\n"
        instructs = "Enter a guess (2-10 or J-A) for Go Fish.\n"
        ret = ("" if self.history != [] else welcome_string) + \
            self.game_state + '\n\n' + \
              ("Result of your last turn: " + self.evaluation + '\n' +
               "Result of Computer's turn: "
               + self.comp_result if self.history != [] else "") + '\n' + \
            self.get_hand() + self.get_pairs() + '\n\n' + \
            "Your Current Score: " + str(self.player_score) + '\n' + \
            'Computer Score: ' + str(self.computer_score) + '\n' + \
              (instructs if self.game_state == 'GAME IN SESSION' else '') +\
            '\nEnter "reset" to reset the current session, ' + \
            'or "clear" to clear Go Fish\'s game history.'
        self.comp_result = ""
        return ret

    def deal(self) -> None:
        """
        Deals 5 cards to the player and the computer
        """
        # Deal 5 cards to both the player and computer
        for iterations in range(5):
            self.player_hand.append(self.deck.pop(0))
            self.computer_hand.append(self.deck.pop(0))

    def get_wins(self) -> int:
        """
        Returns the number of wins that the player has gotten
        """
        return self.wins

    def get_total(self) -> int:
        """
        Returns the number of total games played
        """
        return self.total

    def player_guess(self, guess: str) -> str:
        """
        :param guess: A string that represents the value guessed by
            the player for go_fish
        :return: A string that tells the player if they guessed correctly
            and if the game ended after their guess

        Processes a player's guess in go_fish
        """
        correct = "Guess correct, all cards of value " + guess + " " \
                  "removed from computer hand and added to your hand\n"
        correct_and_player_win = "Guess correct, game over, you win\n"
        correct_and_computer_win = "Guess correct, game over, computer " \
                                   "wins\n"
        val = ""
        tie = "Guess correct, game over, tie\n"

        self.history.append(guess)

        if hand_contains_value(guess, self.computer_hand):
            return self.process_player_guess(correct,
                                             correct_and_computer_win,
                                             correct_and_player_win, guess,
                                             tie)

        else:
            if not self.check_game_over():
                val = self.player_go_fish()
            else:
                return self.check_game()
            self.computer_guess(choice(VALUES))
            return "Go Fish, card of value " + val + " dealt from deck " \
                                                     "and added to your " \
                                                     "hand\n"

    def check_game(self) -> str:
        """
        Helper function for checking who won the game
        """
        if len(self.computer_pairs) < len(self.player_pairs):
            return "You win"
        elif len(self.computer_pairs) > len(self.player_pairs):
            return "Computer wins"
        else:
            return "Game over, tie"

    def process_player_guess(self, correct, correct_and_computer_win,
                             correct_and_player_win, guess, tie) -> str:
        """
        :param correct: String that represents a correct guess
        :param correct_and_computer_win: String that represents a correct guess
            and the computer winnning
        :param correct_and_player_win: String that represents a correct guess
            and the player winnning
        :param guess: A string that represents the player's guess
        :param tie:
        :return: A string that tells the player if they guessed correctly
            and if the game ended after their guess

        Helper function for processing a player's guess
        """
        removed_cards = []
        toggle = True
        for card in self.computer_hand:
            if card[0] == guess:
                if toggle:
                    self.player_score += 1
                    toggle = False
                removed_cards.append(card)
        for card in removed_cards:
            self.computer_hand.remove(card)
            self.player_hand.append(card)
        self.check_pairs()
        if self.check_game_over():
            if len(self.computer_pairs) < len(self.player_pairs):
                self.res = correct_and_player_win
                return correct_and_player_win
            elif len(self.computer_pairs) > len(self.player_pairs):
                self.res = correct_and_computer_win
                return correct_and_computer_win
            else:
                self.res = tie
                return tie
        else:
            self.res = correct
            return correct

    def computer_guess(self, guess: str) -> bool:
        """
        :param guess: A string that represents the value guessed by the computer
            for go_fish
        :return: True if guess is correct, False otherwise
        """
        if self.check_game_over():
            self.evaluation = self.check_game()
            self.game_state = self.check_game()
            return False
        elif hand_contains_value(guess, self.player_hand):
            self.comp_result += "Computer guessed " + guess + \
                                " correctly and took all cards of this value " \
                                "from your hand\n"
            return self.process_computer_guess(guess)
        else:
            if not self.check_game_over():
                self.computer_go_fish()
                self.comp_result += "Computer guessed incorrectly " \
                                    "and drew card from deck\n"
            self.check_pairs()
            return False

    def process_computer_guess(self, guess: str) -> None:
        """
        :param guess: A string that represents the computer's guess
        """
        removed_cards = []
        toggle = True

        for card in self.player_hand:
            if card[0] == guess:
                if toggle:
                    self.computer_score += 1
                    toggle = False
                removed_cards.append(card)
        for card in removed_cards:
            self.player_hand.remove(card)
            self.computer_hand.append(card)
        self.check_pairs()
        return True

    def check_pairs(self) -> None:
        """
        Checks if the player's hand or the computer's hand contains pairs
        """
        # Check if player has pairs
        for card1 in self.player_hand:
            for card2 in self.player_hand:
                if card1 != card2 and card1[0] == card2[0]:
                    if card1 not in self.player_pairs:
                        self.player_pairs.append(card1)
                    if card2 not in self.player_pairs:
                        self.player_pairs.append(card2)

        toggle = True
        self.pair_string = ""
        # Remove pairs from player_hand and add to pairs pile
        for card in self.player_pairs:
            if self.player_hand.__contains__(card):
                if toggle:
                    (val, suit) = card
                    self.pair_string = "You had more than one card of value "\
                                       + val + \
                                       " and all cards of this" \
                                       " value were removed from your hand " \
                                       "and added to your pairs\n"
                    toggle = False

                self.player_hand.remove(card)

        # Check if computer has pairs
        for card1 in self.computer_hand:
            for card2 in self.computer_hand:
                if card1 != card2 and card1[0] == card2[0]:
                    if card1 not in self.computer_pairs:
                        self.computer_pairs.append(card1)
                    if card2 not in self.computer_pairs:
                        self.computer_pairs.append(card2)

        tog = True
        # Remove pairs from computer_hand and add to pairs pile
        for card in self.computer_pairs:
            if self.computer_hand.__contains__(card):
                if tog:
                    (val, suit) = card
                    self.comp_result += "Computer had more than one card of " \
                                        "value " + val + \
                                        " and all cards of this" \
                                        " value were removed from computer hand" \
                                        " and added to computer pairs\n"
                    tog = False

                self.computer_hand.remove(card)

    def check_game_over(self) -> bool:
        """
        :return: True if the game is over, False otherwise

        Checks if the computer or player ran out of cards, or if the deck is empty
        """
        return len(self.player_hand) == 0 or len(self.computer_hand) == 0 \
            or len(self.deck) == 0

    def player_go_fish(self) -> str:
        """
        Deals one card to the player
        """
        if len(self.deck) > 0:
            (val, suit) = self.deck[0]
            self.player_hand.append(self.deck.pop(0))
            return val

    def computer_go_fish(self) -> None:
        """
        Deals one card to the player
        """
        if len(self.deck) > 0:
            self.computer_hand.append(self.deck.pop(0))

    def clear(self) -> None:
        """
        Clears the game's history
        """
        GoFish.all_histories = []
        self.history = []

    def get_pairs(self) -> str:

        if len(self.player_pairs) != 0:
            ret = "\nCurrent Pairs: "
        else:
            return ""

        for (val, suit) in self.player_pairs:
            ret += val + " of " + suit + ". "

        return ret + '\n'

    def reset(self) -> None:
        """
        Restart/reinitialise the game
        """
        self.__init__()

    def get_hand(self) -> str:
        """
        Get the current player hand as a string
        """

        hand_string = "Current Hand: "

        for (val, suit) in self.player_hand:
            hand_string += val + " of " + suit + ". "

        return hand_string
