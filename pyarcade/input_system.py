from typing import Optional, List
import re

from pyarcade.go_fish import VALUES


def validate_mastermind_guess(text_input: str, args: str) -> List[int]:
    """
    :param text_input: str: the string to validate.
    :param args: argument for difficulty
    :return: guess List[int]: a (validated) guess provided as a list of ints.

    Validates the given text input expecting a 4 digit string such as "1234".
    """
    if args == "easy":
        matches = re.search(r"^\d\d\d\d$", text_input)
    elif args == "medium":
        matches = re.search(r"^\d\d\d\d\d$", text_input)
    elif args == "hard":
        matches = re.search(r"^\d\d\d\d\d\d$", text_input)

    if matches:
        return [int(digit) for digit in re.findall(r"\d", text_input)]

    return []


def get_minesweeper_position(text_input: str, height: int,
                             width: int) -> (int, int):
    """
    :param text_input: str: the string to validate.
    :param height: the height of the board.
    :param width: the width of the board.
    :return:
        (int, int): a (validated) tuple of ints if valid input.
        (-1, -1): if invalid input.

    Validates the given text input expecting a format like "2,3" for row 2 and column 3.
    """
    matches = re.search(r"^(\d+),(\d+)(,F|,f)?$", text_input)

    if matches:
        flag = matches.group(3) == ',F' or matches.group(3) == ',f'
        row = int(matches.group(1)) - 1
        col = int(matches.group(2)) - 1
        if 0 <= row < height and 0 <= col < width:
            return row, col, flag

    return -1, -1, False


def validate_connect4_move(string, diff) -> str:
    """
    :param string: the user input.
    :param diff: the difficulty of the current game.
    :return: str or None: either a validated input or None if invalid input.

    Validates a Connect 4 input.
    """
    result_array = [0, 0]
    if diff == "easy":
        pattern = "^(X|Y),([0-6])$"
    elif diff == "medium":
        pattern = "^(X|Y),([0-7])$"
    elif diff == "hard":
        pattern = "^(X|Y),([0-9])$"

    game_input_re = re.compile(pattern)
    result = game_input_re.match(string)
    if string == "reset":
        return string
    elif string == "clear":
        return string
    elif string == "easy" or string == "medium" or string == "hard":
        return string
    elif result is not None:
        result_array[0] = result[1]
        result_array[1] = int(result[2])
        return result_array
    else:
        return None


def validate_go_fish_guess(guess) -> str:
    """
        :param guess: the user input.
        :return: str or None: either a validated input or None if invalid input.

        Validates Go Fish input.
    """
    new_guess = guess.split(' ')

    if len(new_guess) == 2 or len(new_guess) == 1:
        if new_guess[0] in VALUES:
            return new_guess[0]
        else:
            return ""
    else:
        return ""
