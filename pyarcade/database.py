from __future__ import annotations
from datetime import datetime
from threading import Lock
from typing import Optional
import json
import requests


class DatabaseMeta(type):
    """
    A metaclass to be extended by the Database Singleton class.
        This is a thread-safe implementation of Singleton.
    """
    _instance: Optional[Database] = None
    _lock: Lock = Lock()  # lock object that will be used to sync threads

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if not cls._instance:
                cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


class Database(metaclass=DatabaseMeta):
    """
    A Singleton class providing an interface to perform i/o operations on
        the database.
    """

    def __init__(self) -> None:
        self.api_url = 'http://ec2-3-92-200-201.compute-1.amazonaws.com'

    def add_user(self, username: str, password: str) -> bool:
        """
        :param username: username to add.
        :param password: password to add.
        :return: bool: whether add was successful.

        Adds a user to the database.
        """
        response = requests.post(
            url=self.api_url + '/users',
            json={"username": username, "password": password}
        )
        return json.loads(response.content)

    def del_user(self, username: str, password: str) -> bool:
        """
        :param username: username to delete.
        :param password: password to delete.
        :return: bool: whether deletion was successful.

        Deletes a user from the system if the password provided is correct and
        the user exists.
        """
        response_del_logs = self.del_user_logs(username, password)
        response = requests.delete(
            url=self.api_url + '/users/' + username,
            json={"password": password}
        )
        return json.loads(response.content) and response_del_logs

    def get_all_usernames(self) -> list:
        """
        :return: list: a list of all the usernames of PyArcade Online users.
        """
        response = requests.get(url=self.api_url + '/users')
        all_users = json.loads(response.content.decode()[:-1])
        return [user['username'] for user in all_users]

    def is_username_available(self, username: str) -> bool:
        """
        :param username: username to check.
        :return: bool.

        Checks if username is available (not in the database).
        """
        return username not in self.get_all_usernames()

    def is_valid_username_password(self, username: str, password: str) -> bool:
        """
        :param username: username to add.
        :param password: password to add.
        :return: bool.

        Checks if username and password exist as a key-value pair in the
        database.
        """
        response = requests.get(
            url=self.api_url + '/users/' + username,
            json={"password": password}
        )
        return json.loads(response.content)

    def save_score_to_db(self, username: str, password: str, game: str,
                         score: int) -> bool:
        """
        :param username: str: username of the user who is saving the score.
        :param password: str: password of the user who is saving the score.
        :param game: str: the name of the game that was played.
        :param score: int: the score to save.
        :return: bool: whether or not save was successful.

        Saves a score to the database.
        """
        if game not in ["Connect4", "Mastermind", "Minesweeper", "Go Fish"]:
            return False
        response = requests.post(
            url=self.api_url + '/scores/' + username,
            json={"password": password, "game": game, "score": score,
                  "timestamp": str(datetime.now())}
        )
        return json.loads(response.content)

    def get_user_scores(self, username: str, password: str) -> str:
        """
        :param username: str: username of user to show scores of.
        :param password: str: password of user to show scores of.
        :return: str: string with the user's scores.

        Gets the given user's scores as a string.
        """
        response = requests.get(
            url=self.api_url + '/scores/' + username,
            json={"password": password}
        )
        scores = json.loads(response.content)
        return "\n".join(["\tGame: " + score['game'] +
                          ", Score: " + str(score['score']) +
                          ", Timestamp: " + score['timestamp']
                          for score in scores])

    def get_leaderboard(self):
        """
        :return: str: a string describing the top 10 scores in the database.
        """
        response = requests.get(
            url=self.api_url + '/scores',
        )
        scores = json.loads(response.content)
        return "\n".join(["\tUsername: " + score['username'] +
                          ", Game: " + score['game'] +
                          ", Score: " + str(score['score']) +
                          ", Timestamp: " + score['timestamp']
                          for score in scores])

    def change_username(self, username: str, password: str,
                        new_username: str) -> bool:
        """
        :param username: str: username to be changed.
        :param password: str: password of user.
        :param new_username: str: name to switch to.
        :return: bool: whether name was successfully changed.

        Changes the user's display name to the desired name if it's available.
        """
        response = requests.patch(
            self.api_url + '/users/{}'.format(username),
            json={"password": password, "new_username": new_username}
        )
        return json.loads(response.content)

    def add_user_flight_hours(self, username: str, password: str,
                              duration: str) -> bool:
        """
        :param username: str: username of interest.
        :param password: str: password of username of interest.
        :param duration: str: datetime formatted duration.
        :return: bool: whether duration was successfully logged.

        Adds an entry to the user's log of time played on PyArcade.
        """
        response = requests.post(
            url=self.api_url + '/flighthours/' + username,
            json={"password": password, "duration": duration,
                  "timestamp": str(datetime.now())}
        )
        return json.loads(response.content)

    def get_user_flight_hours(self, username: str, password: str) -> list:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: list: list of dictionaries describing the flight hour history.

        Gets a user's flight hours history.
        """
        response = requests.get(
            url=self.api_url + '/flighthours/' + username,
            json={"password": password},
        )
        return json.loads(response.content)

    def del_user_logs(self, username: str, password: str) -> bool:
        """
        :param username: str: username of interest.
        :param password: str: password of user.
        :return: bool: whether deletion was successful.

        Deletes a user's logs (Score History and Flight Hours) from the system if the password provided is correct and
            the user exists.
        """
        return self.del_user_scores(username, password) and \
               self.del_user_flight_hours(username, password)

    def del_user_scores(self, username: str, password: str) -> list:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: bool: whether the deletion was successful.

        Deletes a user's score board.
        """
        response = requests.delete(
            url=self.api_url + '/scores/' + username,
            json={"password": password},
        )
        return json.loads(response.content)

    def del_user_flight_hours(self, username: str, password: str) -> bool:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: bool: whether the deletion was successful.

        Deletes a user's flight hours history.
        """
        response = requests.delete(
            url=self.api_url + '/flighthours/' + username,
            json={"password": password},
        )
        return json.loads(response.content)

    def send_message(self, username: str, password: str, recipient: str,
                     title: str, message: str) -> bool:
        """
        :param username: username of sender.
        :param password: password of sender.
        :param recipient: username of recipient.
        :param title: title of message.
        :param message: content of message.
        :return: bool: whether the message was sent successfully.

        Send a message to another user on PyArcade Online.
        """
        response = requests.post(
            url=self.api_url + '/messages/' + username,
            json={"password": password, "recipient": recipient,
                  "timestamp": str(datetime.now()), "title": title,
                  "message": message}
        )
        return json.loads(response.content)

    def get_inbox_messages(self, username: str, password: str) -> list:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: list: list of dictionaries describing incoming messages.

        Gets a list of dictionaries describing a user's incoming messages.
        """
        response = requests.get(
            url=self.api_url + '/messages/' + username,
            json={"password": password, "in_out": "in"}
        )
        return json.loads(response.content)

    def get_outbox_messages(self, username: str, password: str) -> list:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: list: list of dictionaries describing outgoing messages.

        Gets a list of dictionaries describing a user's outgoing messages.
        """
        response = requests.get(
            url=self.api_url + '/messages/' + username,
            json={"password": password, "in_out": "out"}
        )
        return json.loads(response.content)

    def del_sent_messages(self, username: str, password: str) -> list:
        """
        :param username: str: username of interest.
        :param password: str: password of interest.
        :return: bool: whether the deletion was successful.

        Delete a user's sent messages.
        """
        response = requests.delete(
            url=self.api_url + '/messages/' + username,
            json={"password": password}
        )
        return json.loads(response.content)
