# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8.2

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

ADD . /pyarcade

# Will include in next Sprint if need be. Currently leaving as commented out
# ADD wait-for-it.sh /usr/bin
# RUN chmod u+x /usr/bin/wait-for-it.sh

ENV PYTHONPATH=/pyarcade \
        TERM=xterm

WORKDIR /pyarcade

# COPY pyarcade/resources pyarcade/resources

# Install pip requirements
ADD requirements.txt .
RUN python -m pip install -r requirements.txt && \
    pip install . && make html

#  CMD pyarcade