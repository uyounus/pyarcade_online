import unittest
from pyarcade.database import Database
from pyarcade.inbox_manager import InboxManager


class InboxManagerTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.db = Database()
        self.db.add_user('db_test_user', 'db_test_pass')
        self.db.add_user('db_test_user_1', 'db_test_pass')

    def tearDown(self) -> None:
        self.db.del_sent_messages('db_test_user', 'db_test_pass')
        self.db.del_user_logs('db_test_user', 'db_test_pass')
        self.db.del_user('db_test_user', 'db_test_pass')

        self.db.del_sent_messages('db_test_user_1', 'db_test_pass')
        self.db.del_user_logs('db_test_user_1', 'db_test_pass')
        self.db.del_user('db_test_user_1', 'db_test_pass')

    def test_input_function(self) -> None:
        manager = InboxManager()
        manager.input('send')
        all_users = self.db.get_all_usernames()
        users = ','.join(all_users)
        self.assertEqual(manager.output(),
                         'List of all users in Pyarcade is below:\n'
                         + users +
                         '\n\nTo send a message to a username, '
                         'you need 3 items:' \
                         '\n 1) username\n 2) title of message\n '
                         '3) message to be sent\n\n' \
                         'Format to send message is below '
                         '(without quotation marks):\n'
                         '\"Username_of_recipient:Title_of_message:'
                         'Actual_Message_to_Send\"\n\n'
                         'Type here > '
                         )

    def test_inbox_function(self) -> None:
        manager = InboxManager()
        manager.update_default_data('db_test_user', 'db_test_pass')
        manager.input('inbox')
        self.assertEqual(manager.output(),
                         'All Messages In Inbox Are As Below:\n'
                         + '[]\n' + '\nEnter \"quit\" to return '
                                  'to menu\n> '
                         )

    def test_outbox_function(self) -> None:
        manager = InboxManager()
        manager.update_default_data('db_test_user', 'db_test_pass')
        manager.input('outbox')
        self.assertEqual(manager.output(),
                         'All Sent Messages Are As Per Below:\n'
                         + '[]\n' + '\nEnter \"quit\" to '
                                  'return to menu\n> '
                         )

    def test_send_message_feature(self) -> None:
        manager = InboxManager()
        manager.update_default_data('db_test_user', 'db_test_pass')
        test_string = 'db_test_user_1:test:testing'
        manager.send_message(test_string)
        self.assertEqual(manager.output(),
                         'Message Sent.\n\n'
                         'Enter \"send\" to send a new message\n'
                         'Enter \"inbox\" to view all received messages.\n'
                         'Enter \"outbox\" to view all sent messages\n'
                         'Enter \"delete\" to revert sent messages'
                         ' and clear outbox\n\n'
                         '> '
                         )

    def test_delete_sent_items_feature(self) -> None:
        manager = InboxManager()
        manager.update_default_data('db_test_user', 'db_test_pass')
        test_string = 'db_test_user_1:test:testing'
        manager.send_message(test_string)
        manager.delete_sent_items()

        manager.update_default_data('db_test_user_1', 'db_test_pass')
        manager.show_outbox()
        self.assertEqual(manager.output(),
                         'All Sent Messages Are As Per Below:\n'
                         + '[]\n' + '\nEnter \"quit\" to '
                                  'return to menu\n> '
                         )

    def test_username_validity(self) -> None:
        manager = InboxManager()
        test_string = 'test_mailbox:test:testing'
        self.assertEqual(manager.check_send_username(test_string),
                         False)

    def test_send_message_format_checker(self) -> None:
        manager = InboxManager()
        test_string = 'test_mailbox:test:testing'
        self.assertEqual(manager.check_send_format(test_string),
                         True)

    def test_input_checker_general(self) -> None:
        manager = InboxManager()
        message, boolean = manager.check_input_invalidity('~~')
        self.assertEqual(message, "Invalid Input.\n> ")
        self.assertEqual(boolean, False)

    def test_input_checker_send_command(self) -> None:
        manager = InboxManager()
        manager.input('send')
        test_string = 'test_mailbox:testing'
        message, boolean = manager.check_input_invalidity(test_string)
        self.assertEqual(message, '> Invalid Input.\n> Wrong Username. '
                                  'Please check\n> Wrong Message Format. '
                                  'Please check\n> ')
        self.assertEqual(boolean, False)
