import unittest
from pyarcade.view import View
from pyarcade.database import Database


class ViewTestCase(unittest.TestCase):
    def test1_init_not_crashing(self):
        View()
        view = View(testing=True)
        self.assertEqual(view.test_output, '')
        self.assertEqual(view.test_input, [])

    def test_addstr(self):
        view = View(testing=True)
        view.addstr('Hello')
        self.assertEqual(view.test_output, 'Hello')

    def test_getstr(self):
        test_input = ['Hello']
        view = View(testing=True, test_input=test_input)
        self.assertEqual(view.getstr(), 'Hello')

    def test_clear(self):
        view = View(testing=True)
        view = View(testing=True)
        view.addstr('Hello')
        self.assertEqual(view.test_output, 'Hello')
        view.clear()
        self.assertEqual(view.test_output, '')

    def test_display_main_menu(self):
        view = View(testing=True)
        view.display_main_menu()
        self.assertEqual(view.test_output,
                         "************************\n"
                         "* Welcome to PyArcade! *\n"
                         "************************\n"
                         "\n"
                         "The following games are available:\n"
                         "1. Connect4\n"
                         "2. Go Fish\n"
                         "3. Mastermind\n"
                         "4. Minesweeper\n"
                         "\n"
                         "No user is currently signed in. Enter \"login\" "
                         "to log into your account or enter "
                         "\"create\" to create one.\n"
                         "To play as a guest, select a game by entering its "
                         "number or enter \"quit\" "
                         "to quit PyArcade.\n"
                         "Enter \"leaderboard\" to see the top scores on "
                         "PyArcade Online.\n"
                         "Enter 's' to stop the music or 'p' to play it.\n")

    def test_display(self):
        view = View(testing=True, test_input=['~~', 'quit'])
        view.display()
        self.assertEqual(view.test_output,
                         "************************\n"
                         "* Welcome to PyArcade! *\n"
                         "************************\n"
                         "\n"
                         "The following games are available:\n"
                         "1. Connect4\n"
                         "2. Go Fish\n"
                         "3. Mastermind\n"
                         "4. Minesweeper\n"
                         "\n"
                         "No user is currently signed in. Enter \"login\" "
                         "to log into your account or enter "
                         "\"create\" to create one.\n"
                         "To play as a guest, select a game by entering its "
                         "number or enter \"quit\" "
                         "to quit PyArcade.\n"
                         "Enter \"leaderboard\" to see the top scores on "
                         "PyArcade Online.\n"
                         "Enter 's' to stop the music or 'p' to play it.\n"
                         "> Invalid input.\n> ")

    def test_login(self):
        Database().del_user('view_test_user', 'view_test_pswd')
        Database().add_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['login', 'view_test_user', 'view_test_pswd'])
        view.display()
        Database().del_user('view_test_user', 'view_test_pswd')
        self.assertEqual(view.test_output,

                         "***********************************\n"
                         "****          LOG IN           ****\n"
                         "***********************************\n"
                         "You have logged in successfully.\n"
                         "Enter \"quit\" to return to the main menu.\n> ")

    def test_create_account(self):
        Database().del_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['create', 'view_test_user', 'view_test_pswd',
                                'quit'])
        view.display()
        Database().del_user('view_test_user', 'view_test_pswd')
        self.assertEqual(view.test_output,
                         "************************\n"
                         "* Welcome to PyArcade! *\n"
                         "************************\n"
                         "\n"
                         "The following games are available:\n"
                         "1. Connect4\n"
                         "2. Go Fish\n"
                         "3. Mastermind\n"
                         "4. Minesweeper\n"
                         "\n"
                         "No user is currently signed in. Enter \"login\" "
                         "to log into your account or enter "
                         "\"create\" to create one.\n"
                         "To play as a guest, select a game by entering its "
                         "number or enter \"quit\" "
                         "to quit PyArcade.\n"
                         "Enter \"leaderboard\" to see the top scores on "
                         "PyArcade Online.\n"
                         "Enter 's' to stop the music or 'p' to play it.\n> ")

    def test_change_username(self):
        Database().add_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['login', 'view_test_user', 'view_test_pswd',
                                'quit', 'change name', 'view_test_user2', ''])
        view.display()
        Database().del_user('view_test_user2', 'view_test_pswd')
        self.assertEqual(view.test_output,
                         "************************\n"
                         "* Welcome to PyArcade! *\n"
                         "************************\n"
                         "\n"
                         "The following games are available:\n"
                         "1. Connect4\n"
                         "2. Go Fish\n"
                         "3. Mastermind\n"
                         "4. Minesweeper\n"
                         "\n"
                         "Hi view_test_user2!\n"
                         "Start a game by entering its number or enter "
                         "\"quit\" to quit PyArcade.\n"
                         "Enter \"logout\" to log out of your account.\n\n"
                         "Enter \"scores\" to see your scores.\n"
                         'Enter "delete history" to delete all of your scores and logged time playing Pyarcade.\n'
                         "Enter \"change name\" to change your username.\n"
                         "Enter \"time\" to see your total time spent in Pyarcade.\n"
                         "Enter \"mailbox\" to access your mailbox.\n\n"
                         "Enter \"leaderboard\" to see the top scores on "
                         "PyArcade Online.\n"
                         "Enter 's' to stop the music or 'p' to play it.\n> ")

    def test1_help_game(self):
        view = View(testing=True, test_input=['exit'])
        view.help_game('Go Fish')
        self.assertEqual(view.test_output,
                         '****************************\n'
                         '*        Help Screen      *\n'
                         '****************************\n\n'
                         'The goal of the game is to collect '
                         'a pair of 4 cards having the same number. '
                         'The game is played between 2 players.\n'
                         'All scores are based on the number of correct'
                         ' guesses'
                         ' the user/computer makes' + '\n\n'
                         'PLEASE NOTE:\n(1) Once you play, the computer '
                                                      'will '
                         'automatically play its turn and you will '
                         'be prompted to play again once the computers '
                         'turn is over.\n(2) The player who '
                         'guesses correct when its their turn continues'
                                                      ' to play '
                         'until they make an incorrect guess\n(3) Any '
                                                      'pairs '
                         '(e.g 10 of Hearts, 10 of Diamonds) present '
                                                      'in players '
                         'hand will be removed and '
                         'added to score after current turn''\n\n' +
                         'Instructions: User input should be: '
                         '\"2-10\" or \"J-A\" '
                         '(without the quotation marks).\n'
                         '\nEnter "exit" to return to game.\n'
                         '> ')

    def test_pause_game(self):
        view = View(testing=True, test_input=['~~~', 'quit', 'quit'])
        view.pause_game()
        self.assertEqual(view.test_output, "****************************\n"
                                           "* The Game has been Paused *\n"
                                           "****************************\n"
                                           "\n"
                                           "Enter \"resume\" to continue or "
                                           "\"quit\" to exit the game.\n"
                                           "> Invalid input.\n> ")

    def test_start_game_connect4(self):
        view = View(testing=True, test_input=['quit', 'quit'])
        view.start_game('Connect4')
        print(view.test_output)
        self.assertNotEqual(view.test_output,
                            "**************************\n"
                            "*  Welcome to Connect4!  *\n"
                            "**************************\n"
                            " 0  1  2  3  4  5  6\n"
                            "---------------------\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            " _  _  _  _  _  _  _\n"
                            "---------------------\n"
                            "Enter a move as player X or Y with X, 0"
                            " to drop X in the first column.\n"
                            "Enter \"reset\" to reset the current session, "
                            "or \"clear\" to "
                            "clear Connect4's game history.\n"
                            "\nEnter \"pause\" to take a break.\n"
                            "Enter \"help\" to check the Help Section. \n\n"
                            "Enter \"save and quit\" to save and return to "
                            "the main menu or just \"quit\" to return "
                            "without saving.\n"
                            "To switch to a certain game enter its name.\n"
                            "> ")

    def test_start_game_pause_resume_quit_minesweeper(self):
        view = View(testing=True, test_input=['pause', 'resume', 'quit',
                                              'quit'])
        view.start_game('Minesweeper')
        print(view.test_output)
        self.assertEqual(view.test_output,
                         "GAME IN SESSION\n"
                         "   1  2  3  4  5  6  7  8  9  10\n"
                         "1  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "2  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "3  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "4  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "5  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "6  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "7  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "8  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "9  ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "10 ?  ?  ?  ?  ?  ?  ?  ?  ?  ?\n"
                         "\nCurrent Score: " + str(
                             view.game_manager.game_dictionary[
                                 "Minesweeper"].score) + "\n\n"
                                                         "Difficutly: " +
                         view.game_manager.game_dictionary[
                             "Minesweeper"].difficulty + "\nTo change "
                             "the difficulty and reset the game, enter "
                             "\"easy\", \"medium\", or \"hard\"." + "\n"
                            "Please Note: Changing the difficulty simply increases"
                            "/decreases "
                            "your chances of hitting a mine.\n"
                            "\nEnter \"reset\" to reset the current session.\n"
                            "Enter \"pause\" to take a break.\n"
                            "Enter \"help\" to check the Help Section. \n"
                            "Enter \"save and quit\" to save and return to the "
                            "main menu or just "
                            "\"quit\" to return without saving.\n"
                            "To (switch) to a certain game enter its name "
                            "(Case_Sensitive).\n"
                            "> ")

    def test_show_scores(self):
        Database().del_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['create', 'view_test_user', 'view_test_pswd',
                                'quit', 'login', 'view_test_user',
                                'view_test_pswd', 'quit'])
        view.display()
        view.test_input = ['quit']
        view.show_scores()
        Database().del_user('view_test_user', 'view_test_pswd')
        self.assertEqual(view.test_output,
                         "**************************************\n"
                         "**              SCORES              **\n"
                         "**************************************\n"
                         "\n"
                         "Enter \"quit\" to return to the main menu.\n"
                         "> ")

    def test_show_leaderboard(self):
        Database().del_user("view_test_user", "view_test_pswd")
        view = View(testing=True, test_input=['leaderboard'])
        view.display()
        self.assertEqual(view.test_output[:117],
                         "**************************************\n"
                         "**           LEADERBOARD            **\n"
                         "**************************************\n")

    def test_flight_hours(self):
        Database().del_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['create', 'view_test_user', 'view_test_pswd',
                                'quit', 'login', 'view_test_user',
                                'view_test_pswd', 'quit'])
        view.display()
        view.test_input = ['quit']
        view.show_flight_time()
        Database().del_user('view_test_user', 'view_test_pswd')
        self.assertEqual(view.test_output,
                         "**************************************\n"
                         "**   TOTAL TIME SPENT IN PYARCADE   **\n"
                         "**************************************\n"
                         "You\'ve spent 0 hour(s), 0 minute(s) and 0 second(s) "
                         "in Pyarcade.\n"
                         "\nEnter \"quit\" to return to the main menu.\n"
                         "> ")

    def test_del_log_history(self):
        Database().del_user('view_test_user', 'view_test_pswd')
        view = View(testing=True,
                    test_input=['create', 'view_test_user', 'view_test_pswd',
                                'quit', 'login', 'view_test_user',
                                'view_test_pswd', 'quit'])
        view.display()
        view.test_input = ['quit']
        view.delete_log_history()
        Database().del_user('view_test_user', 'view_test_pswd')
        self.assertEqual(view.test_output,
                         "**************************************\n"
                         "**        User History Deleted      **\n"
                         "*    Please enter 'quit' to return   *\n"
                         "*            to the menu.            *\n"
                         "**************************************\n"
                         "\nEnter \"quit\" to return to the main menu.\n> ")

    def test_mailbox_feature(self):
        view = View(testing=True, test_input=['quit'])
        view.show_mailbox()
        self.assertEqual(view.test_output,
                         '***********************************\n'
                         '****          Mailbox          ****\n'
                         '****   Please enter "quit" to  ****\n'
                         '****     return to last menu   ****\n'
                         '***********************************\n\n'
                         'Enter \"send\" to send a new message\n'
                         'Enter \"inbox\" to view all received messages.\n'
                         'Enter \"outbox\" to view all sent messages\n'
                         'Enter \"delete\" to revert sent messages'
                         ' and clear outbox\n\n'
                         '> '
                         )
