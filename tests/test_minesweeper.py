import unittest
from pyarcade.minesweeper.minesweeper import Minesweeper


class TestMineSweeper(unittest.TestCase):
    def test_square_init(self):
        square = Minesweeper.Square(True)
        self.assertTrue(square.is_mine == True)
        self.assertTrue(square.value == 0)
        self.assertTrue(square.hidden == True)

        square = Minesweeper.Square(False, 4, False)
        self.assertTrue(square.is_mine == False)
        self.assertTrue(square.value == 4)
        self.assertTrue(square.hidden == False)

    def test_square_to_string(self):
        square = Minesweeper.Square(is_mine=True)
        self.assertTrue(str(square) == '?')
        square = Minesweeper.Square(is_mine=True, hidden=False)
        self.assertTrue(str(square) == '*')
        square = Minesweeper.Square(is_mine=False, value=4, hidden=False)
        self.assertTrue(str(square) == '4')
        square = Minesweeper.Square(is_mine=False, value=0, hidden=False)
        self.assertTrue(str(square) == '_')

    def test_minesweeper_init(self):
        minesweeper = Minesweeper()
        self.assertTrue(minesweeper.height == 10)
        self.assertTrue(minesweeper.width == 10)
        self.assertTrue(minesweeper.difficulty == 'easy')
        minesweeper = Minesweeper(difficulty='medium')
        self.assertTrue(minesweeper.difficulty == 'medium')
        minesweeper = Minesweeper(difficulty='hard')
        self.assertTrue(minesweeper.difficulty == 'hard')
        with self.assertRaises(ValueError):
            minesweeper = Minesweeper(difficulty='!@#%$&@%^(!@*&^')

    def test_count_surrounding_mines(self):
        grid = [['mine', 'no mine', 'mine'],
                ['mine', 'no mine', 'no mine'],
                ['no mine', 'no mine', 'no mine']]
        self.assertTrue(Minesweeper.count_surrounding_mines(grid, 1, 1) == 3)
        grid = [['no mine', 'no mine', 'no mine'],
                ['no mine', 'no mine', 'no mine'],
                ['no mine', 'no mine', 'mine']]
        self.assertTrue(Minesweeper.count_surrounding_mines(grid, 1, 1) == 1)
        grid = [['no mine', 'no mine', 'no mine'],
                ['no mine', 'no mine', 'no mine'],
                ['no mine', 'no mine', 'no mine']]
        self.assertTrue(Minesweeper.count_surrounding_mines(grid, 1, 1) == 0)

    def test_get_grid_as_string(self):
        minesweeper = Minesweeper(height=3, width=3)
        self.assertEqual(minesweeper.__str__(), "   1  2  3\n" \
                                                "1  ?  ?  ?\n" \
                                                "2  ?  ?  ?\n" \
                                                "3  ?  ?  ?\n")
        mine = Minesweeper.Square(is_mine=True, hidden=False)
        three = Minesweeper.Square(is_mine=False, value=3, hidden=False)
        zero = Minesweeper.Square(is_mine=False, value=0, hidden=False)
        minesweeper.grid = [[mine, mine, mine],
                            [three, three, three],
                            [zero, zero, zero]]
        self.assertEqual(minesweeper.__str__(), "   1  2  3\n" \
                                                "1  *  *  *\n" \
                                                "2  3  3  3\n" \
                                                "3  _  _  _\n")

    def test_generate_random_grid(self):
        minesweeper = Minesweeper()
        grid = minesweeper.grid
        for row in range(minesweeper.height):
            for col in range(minesweeper.width):
                self.assertTrue(str(grid[row][col]) == '?')
        for row in range(minesweeper.height):
            for col in range(minesweeper.width):
                grid[row][col].unhide()
        for row in range(minesweeper.height):
            for col in range(minesweeper.width):
                self.assertTrue(str(grid[row][col]) == '*'
                                or str(grid[row][col]) == '_'
                                or str(grid[row][col]) == '0'
                                or str(grid[row][col]) == '1'
                                or str(grid[row][col]) == '2'
                                or str(grid[row][col]) == '3'
                                or str(grid[row][col]) == '4'
                                or str(grid[row][col]) == '5'
                                or str(grid[row][col]) == '6'
                                or str(grid[row][col]) == '7'
                                or str(grid[row][col]) == '8')

    def test_only_mines_are_hidden(self):
        minesweeper = Minesweeper(height=3, width=3)
        hidden_mine = Minesweeper.Square(is_mine=True, hidden=True)
        hidden_three = Minesweeper.Square(is_mine=False, value=3, hidden=True)
        seen_zero = Minesweeper.Square(is_mine=False, value=0, hidden=False)
        minesweeper.grid = [[hidden_mine, hidden_mine, hidden_mine],
                            [hidden_three, hidden_three, hidden_three],
                            [seen_zero, seen_zero, seen_zero]]
        self.assertFalse(minesweeper.only_mines_are_hidden())
        minesweeper.grid = [[hidden_mine, hidden_mine, hidden_mine],
                            [seen_zero, seen_zero, seen_zero],
                            [seen_zero, seen_zero, seen_zero]]
        self.assertTrue(minesweeper.only_mines_are_hidden())

    def _prepare_click_square_test(self):
        minesweeper = Minesweeper(height=3, width=3)
        hidden_mine1 = Minesweeper.Square(is_mine=True, hidden=True)
        hidden_mine2 = Minesweeper.Square(is_mine=True, hidden=True)
        hidden_mine3 = Minesweeper.Square(is_mine=True, hidden=True)
        seen_zero = Minesweeper.Square(is_mine=False, value=0, hidden=False)
        hidden_one = Minesweeper.Square(is_mine=False, value=1, hidden=True)
        hidden_two = Minesweeper.Square(is_mine=False, value=2, hidden=True)
        hidden_three = Minesweeper.Square(is_mine=False, value=3, hidden=True)
        minesweeper.grid = [[hidden_mine1, hidden_mine2, hidden_mine3],
                            [hidden_one, hidden_two, hidden_three],
                            [seen_zero, seen_zero, seen_zero]]
        return minesweeper

    def test_click_square_GAME_OVER(self):
        minesweeper = self._prepare_click_square_test()
        minesweeper.click_square(0, 0)
        self.assertEqual(minesweeper.game_state, 'GAME OVER')
        minesweeper = self._prepare_click_square_test()
        minesweeper.click_square(0, 0)
        self.assertEqual(minesweeper.game_state, 'GAME OVER')

    def test_click_square_GAME_WON(self):
        minesweeper = self._prepare_click_square_test()
        seen_zero = Minesweeper.Square(is_mine=False, value=0, hidden=False)
        hidden_zero1 = Minesweeper.Square(is_mine=False, value=0, hidden=True)
        hidden_zero2 = Minesweeper.Square(is_mine=False, value=0, hidden=True)
        minesweeper.grid = [[seen_zero, seen_zero, seen_zero],
                            [seen_zero, seen_zero, seen_zero],
                            [seen_zero, hidden_zero1, hidden_zero2]]
        minesweeper.click_square(2, 1)
        minesweeper.click_square(2, 2)
        self.assertEqual(minesweeper.game_state, 'GAME WON')

    def test_toggle_flag_of_square(self):
        bool_grid = [["mine", "no mine"],
                     ["no mine", "mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.input("1,2,F")  # this calls toggle_flag_of_square and toggle_flag
        self.assertEqual(str(ms.grid[0][1]), '⚑')
        ms.input("1,2,f")  # this calls toggle_flag_of_square and toggle_flag
        self.assertEqual(str(ms.grid[0][1]), '?')

    def test_click_square_when_flagged(self):
        bool_grid = [["mine", "no mine"],
                     ["no mine", "mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.grid[0][1].flagged = True
        self.assertEqual(str(ms.grid[0][1]), '⚑')
        ms.input("1,2")
        self.assertEqual(str(ms.grid[0][1]), '⚑')

    def test_reset_game(self):
        minesweeper = Minesweeper(height=7, width=4, difficulty='hard')
        minesweeper.reset_game()
        self.assertTrue(minesweeper.height == 10)
        self.assertTrue(minesweeper.width == 10)
        self.assertTrue(len(minesweeper.grid) == 10)
        self.assertTrue(len(minesweeper.grid[0]) == 10)
        self.assertTrue(minesweeper.difficulty == 'easy')

    def test_get_grid_as_string_hidden(self):
        minesweeper = Minesweeper(height=7, width=4, difficulty='hard')
        self.assertEqual(minesweeper.__str__(), "   1  2  3  4\n" \
                                                "1  ?  ?  ?  ?\n" \
                                                "2  ?  ?  ?  ?\n" \
                                                "3  ?  ?  ?  ?\n" \
                                                "4  ?  ?  ?  ?\n" \
                                                "5  ?  ?  ?  ?\n" \
                                                "6  ?  ?  ?  ?\n" \
                                                "7  ?  ?  ?  ?\n")

    def test_get_grid_as_string_revealed(self):
        minesweeper = Minesweeper(height=7, width=4, difficulty='hard')
        minesweeper.grid[0][0] = Minesweeper.Square(False, hidden=False)
        self.assertEqual(minesweeper.__str__(), "   1  2  3  4\n" \
                                                "1  _  ?  ?  ?\n" \
                                                "2  ?  ?  ?  ?\n" \
                                                "3  ?  ?  ?  ?\n" \
                                                "4  ?  ?  ?  ?\n" \
                                                "5  ?  ?  ?  ?\n" \
                                                "6  ?  ?  ?  ?\n" \
                                                "7  ?  ?  ?  ?\n")

    def test_populate_grid_with_squares(self):
        bool_grid = [["mine", "no mine"],
                     ["no mine", "mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.grid[0][0].unhide()
        ms.grid[0][1].unhide()
        ms.grid[1][0].unhide()
        ms.grid[1][1].unhide()
        self.assertEqual(ms.__str__(), "   1  2\n" \
                                       "1  *  2\n" \
                                       "2  2  *\n")

    def test_introductory_output(self):
        minesweeper = Minesweeper(height=3, width=3)
        self.assertEqual(minesweeper.output(),
                         '***************************\n'
                         '* Welcome to Minesweeper! *\n'
                         '***************************\n'
                         'GAME IN SESSION\n'
                         '   1  2  3\n'
                         '1  ?  ?  ?\n'
                         '2  ?  ?  ?\n'
                         '3  ?  ?  ?\n'
                         "\nCurrent Score: " + str(minesweeper.score) + "\n\n"
                         "Difficutly: " + minesweeper.difficulty +
                         "\nTo change "
                         "the difficulty and reset the game, enter "
                         "\"easy\", \"medium\", or \"hard\"." + "\n"
                         "Please Note: Changing the difficulty simply "
                         "increases/decreases your chances of hitting "
                         "a mine.\n"
                         '\nEnter \"reset\" to reset the current session.')

    def test_valid_input(self):
        bool_grid = [["mine", "no mine"],
                     ["no mine", "mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.input("1,2")
        self.assertEqual(ms.__str__(), "   1  2\n1  ?  2\n2  ?  "
                                       "?\n")

    def test_invalid_input(self):
        ms = Minesweeper(2, 2)
        ms.input("22,1")
        self.assertEqual(ms.__str__(), "   1  2\n1  ?  ?\n2  ?  "
                                       "?\n")
        ms.input("d;sklfjasdf")
        self.assertEqual(ms.__str__(), "   1  2\n1  ?  ?\n2  ?  "
                                       "?\n")

    def test_reveal_clicked_around(self):
        bool_grid = [["no mine", "no mine"],
                     ["no mine", "no mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.reveal_clicked(0, 0)
        self.assertEqual(str(ms), '   1  2\n1  _  _\n2  _  _\n')

    def test_reveal_clicked_when_not_empty(self):
        bool_grid = [["no mine", "no mine"],
                     ["no mine", "mine"]]
        square_grid = [[None, None], [None, None]]
        ms = Minesweeper(2, 2)
        ms.populate_grid_with_squares(bool_grid, 2, 2, square_grid)
        ms.grid = square_grid
        ms.reveal_clicked(0, 0)
        self.assertEqual(str(ms), '   1  2\n1  1  ?\n2  ?  ?\n')


if __name__ == '__main__':
    unittest.main()
