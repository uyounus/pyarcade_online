import unittest
from pyarcade.connect4.connect4 import Connect4


class Connect4TestCase(unittest.TestCase):
    def test_get_board(self):
        game = Connect4()
        board = game.get_board()
        self.assertEqual(board, [['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_']])

    def test_make_move(self):
        game = Connect4()
        game.move(1, 2)
        game.move(1, 6)
        game.move(2, 6)
        game.move(1, 6)
        game.move(1, 0)
        board = game.get_board()
        self.assertEqual(board, [['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', '_'],
                                 ['_', '_', '_', '_', '_', '_', 1],
                                 ['_', '_', '_', '_', '_', '_', 2],
                                 [1, '_', 1, '_', '_', '_', 1]])

    def test_make_move(self):
        game = Connect4()
        game.input("X,0")
        string_output = "**************************\n" \
                      "*  Welcome to Connect4!  *\n" \
                      "**************************\n"
        for x in range(0, len(game.board[0])):
            string_output += " " + str(x) + " "
        string_output += ('\n---' + ('---' * (len(game.board[0]) - 1)) + "\n")

        for row in range(len(game.board)):
            for col in range(len(game.board[0])):
                string_output += (' %s ' % game.board[row][col])
            string_output += "\n"
        string_output += ('---' + ('---' * (len(game.board[0]) - 1))) + "\n"

        string_output += \
            "Player X Score: " + str(game.x_score) + "\n" \
            "Player Y Score: " + str(game.y_score) + "\n\n" \
            "Difficulty: " + game.difficulty + "\n" \
            "To change the difficulty and reset the game, enter " \
            "\"easy\", \"medium\", or \"hard\"" + "\n\n" \
            "Enter \"reset\" to reset the current session," \
            " or \"clear\" to clear Connect4\'s game history."

        self.assertEqual(game.output(), string_output)


    def test_make_multiple_moves(self):
        game = Connect4()
        game.input("X,0")
        game.input("X,1")
        game.input("X,2")
        game.input("X,3")

        string_output = game.prompt
        for x in range(0, len(game.board[0])):
            string_output += " " + str(x) + " "
        string_output += ('\n---' + ('---' * (len(game.board[0]) - 1)) + "\n")

        for row in range(len(game.board)):
            for col in range(len(game.board[0])):
                string_output += (' %s ' % game.board[row][col])
            string_output += "\n"
        string_output += ('---' + ('---' * (len(game.board[0]) - 1))) + "\n"

        string_output += \
            "Player X Score: " + str(game.x_score) + "\n" \
            "Player Y Score: " + str(game.y_score) + "\n\n" \
            "Difficulty: " + game.difficulty + "\n" \
            "To change the difficulty and reset the game, enter " \
            "\"easy\", \"medium\", or \"hard\"" + "\n" \
            "\nEnter \"reset\" to reset the current session," \
            " or \"clear\" to clear Connect4\'s game history."

        self.assertEqual(game.output(), string_output)

    def test_evaluate_won_game(self):
        game = Connect4()
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 0, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 3, 3, 4],
                      [1, 2, 2, 2, 3, 3, 4]]
        self.assertTrue(game.evaluate_game(1))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 3, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 3, 3, 4],
                      [1, 2, 2, 2, 3, 3, 4]]
        self.assertTrue(game.evaluate_game(3))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 2, 5, 3, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 2, 3, 4],
                      [1, 2, 2, 2, 3, 2, 4]]
        self.assertTrue(game.evaluate_game(2))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 5, 5, 5, 5],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 2, 3, 4],
                      [1, 2, 2, 2, 3, 2, 4]]
        self.assertTrue(game.evaluate_game(5))

    def test_evaluate_lost_game(self):
        game = Connect4()
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 0, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 3, 3, 4],
                      [1, 2, 2, 2, 3, 3, 4]]
        self.assertFalse(game.evaluate_game(3))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 3, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 3, 3, 4],
                      [1, 2, 2, 2, 3, 3, 4]]
        self.assertFalse(game.evaluate_game("X"))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 2, 5, 3, 0, 0],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 2, 3, 4],
                      [1, 2, 2, 2, 3, 2, 4]]
        self.assertFalse(game.evaluate_game(1))
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 5, 5, 5, 5],
                      [0, 0, 1, 2, 3, 3, "X"],
                      [0, 1, 2, 2, 2, 3, 4],
                      [1, 2, 2, 2, 3, 2, 4]]
        self.assertFalse(game.evaluate_game(2))

    def test_game_reset(self):
        game = Connect4()
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 0, 0, 0],
                      [0, 0, 1, 2, 0, 0, 0],
                      [0, 1, 2, 2, 0, 0, 0],
                      [1, 2, 2, 2, 0, 0, 0]]
        game.reset()
        self.assertEqual(game.board, [['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_']])

    def test_game_clear(self):
        game = Connect4()
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 0, 0, 0],
                      [0, 0, 1, 2, 0, 0, 0],
                      [0, 1, 2, 2, 0, 0, 0],
                      [1, 2, 2, 2, 0, 0, 0]]
        game.evaluate_game(1)
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 2, 0, 3, 0, 0, 0],
                      [0, 3, 2, 2, 0, 0, 0],
                      [0, 3, 2, 2, 0, 0, 0],
                      [3, 3, 2, 2, 2, 0, 0]]
        game.evaluate_game(2)
        game_history = game.game_storage
        self.assertEqual(game_history, [[[0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 1, 0, 0, 0],
                                         [0, 0, 1, 2, 0, 0, 0],
                                         [0, 1, 2, 2, 0, 0, 0],
                                         [1, 2, 2, 2, 0, 0, 0]],
                                        [[0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 2, 0, 3, 0, 0, 0],
                                         [0, 3, 2, 2, 0, 0, 0],
                                         [0, 3, 2, 2, 0, 0, 0],
                                         [3, 3, 2, 2, 2, 0, 0]]])
        game.clear()
        self.assertEqual(game.game_storage, [])

    def test_print_board(self):
        game = Connect4()
        game.print_board()
        self.assertEqual(game.board, [['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_'],
                                     ['_', '_', '_', '_', '_', '_', '_']])

    def test_game_storage(self):
        game = Connect4()
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 1, 0, 0, 0],
                      [0, 0, 1, 2, 0, 0, 0],
                      [0, 1, 2, 2, 0, 0, 0],
                      [1, 2, 2, 2, 0, 0, 0]]
        game.evaluate_game(1)
        game.board = [[0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 0, 0, 0, 0, 0, 0],
                      [0, 3, 0, 3, 0, 0, 0],
                      [0, 3, 0, 2, 0, 0, 0],
                      [0, 3, 0, 2, 0, 0, 0],
                      [3, 3, 0, 2, 2, 0, 0]]
        game.evaluate_game(3)
        game_history = game.game_storage
        self.assertEqual(game_history, [[[0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 1, 0, 0, 0],
                                         [0, 0, 1, 2, 0, 0, 0],
                                         [0, 1, 2, 2, 0, 0, 0],
                                         [1, 2, 2, 2, 0, 0, 0]],
                                        [[0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0],
                                         [0, 3, 0, 3, 0, 0, 0],
                                         [0, 3, 0, 2, 0, 0, 0],
                                         [0, 3, 0, 2, 0, 0, 0],
                                         [3, 3, 0, 2, 2, 0, 0]]])


if __name__ == '__main__':
    unittest.main()
