import unittest
from pyarcade.mastermind.mastermind import Mastermind


class TestMastermind(unittest.TestCase):
    def test_init(self):
        game = Mastermind()
        Mastermind.all_histories = []
        self.assertEqual(len(game.hidden_sequence), 4)
        self.assertEqual(game.history, [])
        self.assertEqual(Mastermind.all_histories, [])
        self.assertEqual(game.evaluation, "")

    def test_generate_hidden_sequence(self):
        game = Mastermind()
        self.assertEqual(len(game.generate_hidden_sequence()), 4)

    def test_update_hidden_sequence(self):
        game = Mastermind("medium")
        game.generate_hidden_sequence()
        game = Mastermind("hard")
        game.generate_hidden_sequence()
        game = Mastermind()

        old_hidden_seq = game.hidden_sequence
        game.update_hidden_sequence()
        new_hidden_seq = game.hidden_sequence
        self.assertNotEqual(old_hidden_seq, new_hidden_seq)

    def test_eval_guess_exact_match(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        self.assertEqual(game.eval_guess([1, 2, 3, 4]),
                         "Evaluation:\n\n"
                         "1-✔\n"
                         "2-✔\n"
                         "3-✔\n"
                         "4-✔\n")

    def test_eval_guess_incorrect_position(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        self.assertEqual(game.eval_guess([1, 2, 2, 4]),
                         "Evaluation:\n\n"
                         "1-✔\n"
                         "2-✔\n"
                         "2-≈\n"
                         "4-✔\n")

    def test_eval_guess_incorrect_position_and_digit(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        self.assertEqual(game.eval_guess([0, 2, 2, 4]),
                         "Evaluation:\n\n"
                         "0-✘\n"
                         "2-✔\n"
                         "2-≈\n"
                         "4-✔\n")

    def test_history_and_histories(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        game.input("1224")
        game.input("0224")
        game.input("1234")
        self.assertEqual(game.history, [['1-✔', '2-✔', '2-≈', '4-✔'],
                                        ['0-✘', '2-✔', '2-≈', '4-✔'],
                                        ['1-✔', '2-✔', '3-✔', '4-✔']])
        self.assertEqual(Mastermind.all_histories,
                         [[['1-✔', '2-✔', '2-≈', '4-✔'],
                           ['0-✘', '2-✔', '2-≈', '4-✔'],
                           ['1-✔', '2-✔', '3-✔', '4-✔']]])

    def test_invalid_input(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]

        game.input("")
        game.input("safdasdf")
        game.input("123")
        game.input("1234sdf")
        game.input("sdad1234sdf")
        self.assertEqual(game.evaluation, "")
        self.assertEqual(game.history, [])
        self.assertEqual(Mastermind.all_histories, [])

    def test_valid_input_not_exact_match(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        game.input("0224")
        self.assertEqual(game.evaluation,
                         "Evaluation:\n\n"
                         "0-✘\n"
                         "2-✔\n"
                         "2-≈\n"
                         "4-✔\n")
        self.assertEqual(game.history, [['0-✘', '2-✔', '2-≈', '4-✔']])
        self.assertEqual(Mastermind.all_histories, [])

    def test_valid_input_exact_match(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        game.input("1234")
        self.assertEqual(game.evaluation,
                         "Evaluation:\n\n"
                         "1-✔\n"
                         "2-✔\n"
                         "3-✔\n"
                         "4-✔\n")
        self.assertEqual(game.history, [['1-✔', '2-✔', '3-✔', '4-✔']])
        self.assertEqual(Mastermind.all_histories,
                         [[['1-✔', '2-✔', '3-✔', '4-✔']]])

    def test_valid_input_reset(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        game.input('reset')
        self.assertEqual(game.history, [])

    def test_valid_input_clear(self):
        game = Mastermind()
        Mastermind.all_histories = []
        game.hidden_sequence = [1, 2, 3, 4]
        game.input('0224')
        game.input('1234')
        self.assertEqual(Mastermind.all_histories, [
            [['0-✘', '2-✔', '2-≈', '4-✔'], ['1-✔', '2-✔', '3-✔', '4-✔']]])
        game.input('clear')
        self.assertEqual(Mastermind.all_histories, [])

    def test_get_score(self):
        game = Mastermind()
        self.assertEqual(game.get_score(), 0)

    def test_output(self):
        game = Mastermind()
        welcome_string = "**************************\n" \
                         "* Welcome to Mastermind! *\n" \
                         "**************************\n"
        if game.difficulty == "easy":
            prompt = "\"1234\""
        elif game.difficulty == "medium":
            prompt = "\"12345\""
        elif game.difficulty == "hard":
            prompt = "\"123456\""

        instruct = "Enter digits to guess the hidden sequence (e.g. " \
                   + prompt + ")).\n" \
                              "'✘' means bad digit, '≈' means incorrect " \
                              "position, '✔' means correct guess.\n"
        self.assertEqual(game.output(), ("" if game.history != []
                                         else welcome_string) +
                         game.game_state + '\n' +
                         game.evaluation + '\n' +
                         "Current Score: " + str(game.score) + '\n' +
                         "Difficulty: " + game.difficulty +
                         "\nTo change the difficulty and reset the game,"
                         " enter \"easy\", \"medium\", or \"hard\"" + "\n\n" +
                         instruct +
                         "\nEnter \"reset\" to reset the current session, " +
                         "or \"clear\" to clear Mastermind\'s game history.")

    def test_GAME_WON(self):
        game = Mastermind()
        game.hidden_sequence = [1, 2, 3, 4]
        ret = game.eval_guess([1, 2, 3, 4])
        self.assertEqual(ret, "Evaluation:\n\n1-✔\n2-✔\n3-✔\n4-✔\n")
        self.assertEqual(game.game_state, 'GAME WON')

    def test_GAME_OVER(self):
        game = Mastermind()
        game.hidden_sequence = [1, 2, 3, 4]
        for i in range(15):
            game.input("1111")
        self.assertEqual(game.game_state, 'GAME OVER')


if __name__ == '__main__':
    unittest.main()
