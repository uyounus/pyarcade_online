import unittest

from pyarcade.game_manager import GameManager
from pyarcade.mastermind.mastermind import Mastermind


class GameManagerTestCase(unittest.TestCase):
    def test_init(self):
        game_manager = GameManager()
        self.assertEqual(game_manager.state, None)
        self.assertEqual(len(game_manager.game_dictionary), 4)

    def test_reset_game_list(self):
        game_manager = GameManager()
        self.assertNotEqual(game_manager.game_dictionary, {})
        game_manager.reset_game_list()
        self.assertEqual(game_manager.game_dictionary, {})

    def test_game_manager_connect4(self):
        game_manager = GameManager()
        game_manager.set_state("Connect4")
        game_manager.input("X,1")
        result = game_manager.output()
        expected = "**************************\n" \
                   "*  Welcome to Connect4!  *\n" \
                   "**************************\n" \
                    " 0  1  2  3  4  5  6 \n" \
                    "---------------------\n" \
                    " _  _  _  _  _  _  _ \n" \
                    " _  _  _  _  _  _  _ \n" \
                    " _  _  _  _  _  _  _ \n" \
                    " _  _  _  _  _  _  _ \n" \
                    " _  _  _  _  _  _  _ \n" \
                    " _  X  _  _  _  _  _ \n" \
                    "---------------------\n" \
                   "Player X Score: " + str(game_manager.game_dictionary["Connect4"].x_score) + "\n" \
                   "Player Y Score: " + str(game_manager.game_dictionary["Connect4"].y_score) + "\n\n" \
                   "Difficulty: " + game_manager.game_dictionary["Connect4"].difficulty + "\n" \
                   "To change the difficulty and reset the game, enter " \
                   "\"easy\", \"medium\", or \"hard\"" + "\n\n" \
                   "Enter \"reset\" to reset the current session, " \
                   "or \"clear\" to clear " \
                   "Connect4's game history."
        self.assertEqual(result, expected)

    def test_input(self):
        game_manager = GameManager()
        game_manager.set_state("Connect4")
        self.assertEqual(game_manager.state_name, 'Connect4')
        game_manager.input("Mastermind")
        self.assertEqual(game_manager.state_name, 'Mastermind')
        self.assertEqual(type(game_manager.state), type(Mastermind()))

    def test_game_list(self):
        game_manager = GameManager()
        self.assertEqual(game_manager.game_list(),
                         ["Connect4", "Go Fish", "Mastermind", "Minesweeper"])

    def test_reset_state(self):
        game_manager = GameManager()
        game_manager.set_state("Connect4")
        old_connect4 = game_manager.game_dictionary["Connect4"]
        game_manager.reset_state()
        self.assertEqual(type(game_manager.game_dictionary["Connect4"]),
                         type(old_connect4))
        self.assertNotEqual(game_manager.game_dictionary["Connect4"],
                            old_connect4)
