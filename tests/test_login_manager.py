import unittest

from pyarcade.database import Database
from pyarcade.login_manager import LoginManager


class LoginManagerTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.db = Database()
        self.db.add_user('db_test_user', 'db_test_pass')

    def tearDown(self) -> None:
        self.db.del_sent_messages('db_test_user', 'db_test_pass')
        self.db.del_user_logs('db_test_user', 'db_test_pass')
        self.db.del_user('db_test_user', 'db_test_pass')

    def test_init_not_crashing(self):
        manager = LoginManager()
        self.assertEqual(manager.logged_in, False)

    def test_initial_output(self):
        manager = LoginManager()
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "Please enter your username.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")

    def test_user_entered_valid_input(self):
        manager = LoginManager()
        manager.input('db_test_user')
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "Hello db_test_user, please enter your password.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")

    def test_user_entered_invalid_input(self):
        manager = LoginManager()
        manager.input('SOME INVALID USERNAME')
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "The username does not exist, try again.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")

    def test_successful_login_output(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('db_test_pass')
        manager.input('asdklfja;sldfj;laskjdf;lksfdsdfl;kj')
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "You have logged in successfully.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")

    def test_is_logged_in_false(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('SOME INCORRECT PASSWORD')
        self.assertEqual(manager.is_logged_in(), False)

    def test_is_logged_in_True(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('db_test_pass')
        self.assertEqual(manager.is_logged_in(), True)

    def test_logout(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('db_test_pass')
        manager.log_out()
        self.assertEqual(manager.is_logged_in(), False)

    def test_get_curr_user(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('db_test_pass')
        self.assertEqual(manager.get_curr_user(), "db_test_user")

    def test_get_curr_pswd(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('db_test_pass')
        self.assertEqual(manager.get_curr_pswd(), "db_test_pass")

    def test_incorrect_password(self):
        manager = LoginManager()
        manager.input('db_test_user')
        manager.input('SOME INCORRECT PASSWORD')
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "Incorrect password, try again.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")

    def test_username_not_exist(self):
        manager = LoginManager()
        manager.input('~~~~~~ SOME NOT EXISTING USERNAME ~~~~~~~')
        self.assertEqual(manager.output(),
                         "***********************************\n" \
                         "****          LOG IN           ****\n" \
                         "***********************************\n" \
                         "The username does not exist, try again.\n" \
                         'Enter "quit" to return to the main menu.\n' \
                         "> ")
