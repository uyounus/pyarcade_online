import unittest
from pyarcade.accountManager import AccountManager


class AccountManagerTestCase(unittest.TestCase):
    def test_init(self):
        acc_manager = AccountManager()
        self.assertEqual("", acc_manager.currentUsername)
        self.assertEqual(False, acc_manager.passwordAccepted)
        self.assertEqual(False, acc_manager.usernameAccepted)

    def test_create_account(self):
        acc_manager = AccountManager()
        acc_manager.create_account("test_create", "Password")
        self.assertEqual(acc_manager.output(),
                         "***********************************\n" \
                         "****     CREATE AN ACCOUNT     ****\n" \
                         "***********************************\n" \
                         "***********************************\n" \
                         "**** Enter a username and then ****\n" \
                         "****     enter a password.     ****\n" \
                         "***********************************\n\n" \
                         "Username: ")
        self.assertFalse(acc_manager.database.is_username_available("test_create"))
        acc_manager.database.del_user("test_create", "Password")

    def test_account_creation_success(self):
        acc_manager = AccountManager()
        acc_manager.database.del_user("Ryan", "Password123")
        acc_manager.input("Ryan")
        self.assertEqual(acc_manager.output(),
                         "***********************************\n" \
                         "****     CREATE AN ACCOUNT     ****\n" \
                         "***********************************\n" \
                         "***********************************\n" \
                         "**** Enter a username and then ****\n" \
                         "****     enter a password.     ****\n" \
                         "***********************************\n\n" \
                         "Password: ")
        acc_manager.input("Password123")
        acc_manager.database.del_user("Ryan", "Password123")
        self.assertTrue(acc_manager.account_created_successfully())

    def test_username_valid(self):
        acc_manager = AccountManager()
        self.assertTrue(acc_manager.validate_username("Steve"))

    def test_username_taken(self):
        acc_manager = AccountManager()
        acc_manager.database.del_user("Mike", "Password")
        acc_manager.input("Mike")
        acc_manager.input("Password")
        acc_manager.__init__()
        acc_manager.input("Mike")
        acc_manager.database.del_user("Mike", "Password")
        self.assertEqual(acc_manager.output(),
                         "***********************************\n" \
                         "****     CREATE AN ACCOUNT     ****\n" \
                         "***********************************\n" \
                         "***********************************\n" \
                         "**** Enter a username and then ****\n" \
                         "****     enter a password.     ****\n" \
                         "***********************************\n\n" \
                         "Sorry, that username is taken. Please enter"
                         " another.\n" \
                         "Username: ")
        self.assertTrue(acc_manager.usernameTaken)
