import unittest
from pyarcade.input_system import *


class TestInputSystem(unittest.TestCase):

    def test_validate_mastermind_guess_accept(self):
        self.assertEqual(validate_mastermind_guess('1234', "easy"), [1, 2, 3, 4])
        self.assertEqual(validate_mastermind_guess('4321', "easy"), [4, 3, 2, 1])
        self.assertEqual(validate_mastermind_guess('12345', "medium"), [1, 2, 3, 4, 5])
        self.assertEqual(validate_mastermind_guess('674321', "hard"), [6, 7, 4, 3, 2, 1])

    def test_validate_mastermind_guess_fail(self):
        self.assertEqual(validate_mastermind_guess('', "easy"), [])
        self.assertEqual(validate_mastermind_guess('4321asdf', "easy"), [])
        self.assertEqual(validate_mastermind_guess('sfdf1234', "easy"), [])
        self.assertEqual(validate_mastermind_guess('sfdfadsg', "easy"), [])

    def test_get_minesweeper_position_accept(self):
        # Keep in mind that 1 is subtracted before returning indices
        self.assertEqual(get_minesweeper_position('1,1', 10, 10),
                         (0, 0, False))
        self.assertEqual(get_minesweeper_position('3,2', 10, 10),
                         (2, 1, False))
        self.assertEqual(get_minesweeper_position('5,9', 10, 10),
                         (4, 8, False))
        self.assertEqual(get_minesweeper_position('005,009', 10, 10),
                         (4, 8, False))

    def test_get_minesweeper_position_fail(self):
        self.assertEqual(get_minesweeper_position('', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('123133', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('dfsadf', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('1,2a#$', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('sf$1,2', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('22,222', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('-1,1', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('1,,1', 10, 10),
                         (-1, -1, False))
        self.assertEqual(get_minesweeper_position('0,0', 10, 10),
                         (-1, -1, False))

    def test_get_minesweeper_position_toggle_flag(self):
        self.assertEqual(get_minesweeper_position('1,1,F', 10, 10),
                         (0, 0, True))
        self.assertEqual(get_minesweeper_position('2,3,f', 10, 10),
                         (1, 2, True))

    def test_validate_connect4_move_accept(self):
        self.assertEqual(validate_connect4_move("1,1", "easy"), None)
        self.assertEqual(validate_connect4_move("X,6", "easy"), ["X", 6])
        self.assertEqual(validate_connect4_move("X,0", "easy"), ["X", 0])
        self.assertEqual(validate_connect4_move("X,6", "medium"), ["X", 6])
        self.assertEqual(validate_connect4_move("X,0", "hard"), ["X", 0])
        self.assertEqual(validate_connect4_move("clear", "hard"), "clear")
        self.assertEqual(validate_connect4_move("reset", "hard"), "reset")
        self.assertEqual(validate_connect4_move("easy", "hard"), "easy")

    def test_validate_connect4_move_fail(self):
        self.assertEqual(validate_connect4_move("Token,7", "easy"), None)
        self.assertEqual(validate_connect4_move("Xkdsfds6", "easy"), None)
        self.assertEqual(validate_connect4_move("", "easy"), None)
        self.assertEqual(validate_connect4_move("ddddXkdsfds63,4", "easy"), None)

    def test_validate_go_fish_guess_accept(self):
        self.assertEqual(validate_go_fish_guess("2"), "2")
        self.assertEqual(validate_go_fish_guess("J"), "J")
        self.assertEqual(validate_go_fish_guess("A"), "A")

    def test_validate_fo_fish_guess_fail(self):
        self.assertEqual(validate_go_fish_guess("E"), "")
        self.assertEqual(validate_go_fish_guess("X,6"), "")
        self.assertEqual(validate_go_fish_guess("a"), "")
        self.assertEqual(validate_go_fish_guess("a b c"), "")


if __name__ == '__main__':
    unittest.main()
