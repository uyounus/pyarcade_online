import unittest
from pyarcade.database import DatabaseMeta
from pyarcade.database import Database
from pathlib import Path
import json


class DatabaseTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.db = Database()
        self.db.add_user('db_test_user', 'db_test_pass')

    def tearDown(self) -> None:
        self.db.del_sent_messages('db_test_user', 'db_test_pass')
        self.db.del_user_logs('db_test_user', 'db_test_pass')
        self.db.del_user('db_test_user', 'db_test_pass')

    def test_can_see_users(self):
        self.db.add_user('db_test_user2', 'db_test_pass')
        self.assertTrue('db_test_user' in self.db.get_all_usernames())
        self.assertTrue('db_test_user2' in self.db.get_all_usernames())
        self.db.del_user('db_test_user2', 'db_test_pass')

    def test_add_user(self):
        # already added a user in set up so checking that they exist
        self.assertTrue('db_test_user' in self.db.get_all_usernames())

    def test_is_username_available(self):
        self.assertEqual(self.db.is_username_available('db_test_user'), False)
        self.assertEqual(self.db.is_username_available('~~!~!~!!~'), True)

    def test_del_user(self):  # included 'z' so this runs last
        # already added a user in set up so checking that can delete them
        self.assertEqual(self.db.is_username_available('db_test_user'), False)
        self.assertEqual(self.db.del_user('db_test_user', 'db_test_pass'),
                         True)
        self.assertEqual(self.db.is_username_available('db_test_user'), True)

    def test_is_valid_username_password(self):
        self.assertEqual(self.db.is_valid_username_password('db_test_user',
                                                            'db_test_pass'),
                         True)
        self.assertEqual(self.db.is_valid_username_password('db_test_user',
                                                            '~~~~~~~~~~~~'),
                         False)

    def test_save_score_to_db_success(self):
        self.assertEqual(self.db.save_score_to_db('db_test_user',
                                                  'db_test_pass',
                                                  'Mastermind', 50), True)

    def test_save_score_to_db_fail(self):
        self.assertEqual(self.db.save_score_to_db('db_test_user',
                                                  'WRONG_PASSWORD',
                                                  'Mastermind', 50), False)

    def test_get_user_scores_success(self):
        self.db.save_score_to_db('db_test_user', 'db_test_pass',
                                 'Mastermind', 50)
        self.assertEqual(self.db.get_user_scores('db_test_user',
                                                 'db_test_pass')[:40],
                         "\tGame: Mastermind, Score: 50, Timestamp:")

    def test_get_user_scores_fail(self):
        self.assertEqual(self.db.get_user_scores('db_test_user',
                                                 'WRONG_PASSWORD'), "")

    def test_del_user_scores(self):
        self.db.save_score_to_db('db_test_user', 'db_test_pass',
                                 'Mastermind', 50)
        self.assertEqual(self.db.get_user_scores('db_test_user',
                                                 'db_test_pass')[:40],
                         "\tGame: Mastermind, Score: 50, Timestamp:")
        self.db.del_user_scores('db_test_user', 'db_test_pass')
        self.assertEqual(self.db.get_user_scores('db_test_user',
                                                 'db_test_pass'), "")

    def test_get_leaderboard(self):
        self.db.save_score_to_db('db_test_user', 'db_test_pass',
                                 'Mastermind', 1)
        self.db.save_score_to_db('db_test_user', 'db_test_pass',
                                 'Mastermind', 55)
        self.assertEqual(self.db.get_leaderboard()[:64],
                         "\tUsername: db_test_user, Game: "
                         "Mastermind, Score: 55, Timestamp:")

    def test_change_name_pass(self):
        self.assertEqual(True, self.db.change_username("db_test_user",
                                                       "db_test_pass",
                                                       "db_test_user_new"))
        self.assertTrue('db_test_user_new' in self.db.get_all_usernames())
        self.assertEqual(True, self.db.change_username("db_test_user_new",
                                                       "db_test_pass",
                                                       "db_test_user"))

    def test_change_name_fail(self):
        self.assertEqual(False, self.db.change_username("db_test_user",
                                                        "db_test_pass_WRONG",
                                                        "db_test_user_new"))
        self.assertEqual(False, 'db_test_user_new' in
                         self.db.get_all_usernames())

    def test_add_user_flight_hours(self):
        self.assertEqual(self.db.add_user_flight_hours("db_test_user",
                                                       "db_test_pass",
                                                       "5 hours"), True)

    def test_get_user_flight_hours(self):
        self.db.add_user_flight_hours("db_test_user", "db_test_pass",
                                      "5 hours")
        res = self.db.get_user_flight_hours("db_test_user", "db_test_pass")
        self.assertEqual(res[0]['duration'], "5 hours")

    def test_del_user_flight_hours(self):
        self.db.add_user_flight_hours("db_test_user", "db_test_pass",
                                      "5 hours")
        res = self.db.get_user_flight_hours("db_test_user", "db_test_pass")
        self.assertEqual(res[0]['duration'], "5 hours")
        self.assertEqual(self.db.del_user_flight_hours("db_test_user",
                                                       "db_test_pass"), True)
        res = self.db.get_user_flight_hours("db_test_user", "db_test_pass")
        self.assertEqual([], res)

    def test_delete_user_logs_pass(self):
        self.db.add_user_flight_hours("db_test_user", "db_test_pass",
                                      "5 hours")
        self.db.save_score_to_db('db_test_user', 'db_test_pass',
                                 'Mastermind', 50)
        self.assertEqual(self.db.del_user_logs("db_test_user", "db_test_pass"),
                         True)
        res = self.db.get_user_flight_hours("db_test_user", "db_test_pass")
        self.assertEqual([], res)
        self.assertEqual(self.db.get_user_scores('db_test_user',
                                                 'db_test_pass'), "")

    def test_delete_user_logs_fail(self):
        self.assertEqual(self.db.del_user_logs("db_test_user",
                                               "db_WRONG_PASSWORD"),
                         False)

    def test_send_message(self):
        self.assertEqual(self.db.send_message("db_test_user", "db_test_pass",
                                              "some_recipient", "some title",
                                              "some message"), True)

    def test_get_inbox_messages(self):
        self.db.add_user("some_recipient", "~")
        self.db.send_message("db_test_user", "db_test_pass",
                             "some_recipient", "some title", "some message")
        res = self.db.get_inbox_messages("some_recipient", "~")
        self.db.del_user("some_recipient", "~")
        self.assertEqual(res[0]['recipient'], "some_recipient")

    def test_get_outbox_messages(self):
        self.db.add_user("some_recipient", "~")
        self.db.send_message("db_test_user", "db_test_pass",
                             "some_recipient", "some title", "some message")
        res = self.db.get_outbox_messages("db_test_user", "db_test_pass")
        self.assertEqual(res[0]['sender'], "db_test_user")
        self.db.del_user("some_recipient", "~")

    def test_del_sent_messages(self):
        self.db.send_message("db_test_user", "db_test_pass",
                             "some_recipient", "some title", "some message")
        res = self.db.get_outbox_messages("db_test_user", "db_test_pass")
        self.assertEqual(res[0]['sender'], "db_test_user")
        self.assertEqual(self.db.del_sent_messages("db_test_user",
                                                   "db_test_pass"), True)
        res = self.db.get_outbox_messages("db_test_user", "db_test_pass")
        self.assertEqual(res, [])
