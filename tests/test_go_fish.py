import unittest

from pyarcade.go_fish import SUITS, VALUES
from pyarcade.go_fish.go_fish import GoFish


class GoFishTestCase(unittest.TestCase):
    def test_deal_method(self):
        game = GoFish()
        self.assertEqual(len(game.player_hand), 5)
        self.assertEqual(len(game.computer_hand), 5)

    def test_correct_guess_player(self):
        game = GoFish()
        res = game.player_guess((game.computer_hand[0])[0])
        self.assertEqual(res, game.res)

    def test_correct_guess_computer(self):
        game = GoFish()
        self.assertTrue(game.computer_guess((game.player_hand[0])[0]))

    def test_incorrect_guess_player(self):
        game = GoFish()
        toggle = True
        for suit in SUITS:
            for val in VALUES:
                if [val, suit] not in game.computer_hand:
                    for card in game.computer_hand:
                        if card[0] == val:
                            toggle = False
                    if toggle:
                        value = game.deck[0][0]
                        self.assertEqual(game.player_guess(val), "Go Fish, card of value " + value +
                                         " dealt from deck and added to your hand\n")
                    break
            break

    def test_check_pairs(self):
        game = GoFish()
        game.player_hand = [("2", "Hearts"), ("2", "Spades")]
        game.computer_hand = [("3", "Hearts"), ("3", "Spades")]
        game.check_pairs()
        self.assertEqual(len(game.player_pairs), 2)
        self.assertEqual(len(game.computer_pairs), 2)

    def test_check_pairs2(self):
        game = GoFish()
        game.player_hand = [("2", "Hearts")]
        game.computer_hand = [("3", "Hearts")]
        game.check_pairs()
        self.assertEqual(len(game.player_pairs), 0)
        self.assertEqual(len(game.computer_pairs), 0)

    def test_incorrect_guess_computer(self):
        game = GoFish()

        for suit in SUITS:
            for val in VALUES:
                toggle = True
                for [val2, suit2] in game.player_hand:
                    if val2 == val:
                        toggle = False
                if toggle:
                    self.assertEqual(game.computer_guess(val), False)
                    break
            break

    def test_game_over(self):
        game = GoFish()
        game.player_hand = []
        game.computer_hand = []
        game.deck = []
        self.assertEqual(game.check_game_over(), True)

    def test_game_over2(self):
        game = GoFish()
        game.computer_hand = []
        self.assertEqual(game.check_game_over(), True)

    def test_game_over3(self):
        game = GoFish()
        game.deck = []
        self.assertEqual(game.check_game_over(), True)

    def test_game_clear(self):
        game = GoFish()
        game.reset()
        self.assertEqual(game.check_game_over(), True)
        self.assertEqual(game.player_hand, [])
        self.assertEqual(game.computer_hand, [])
        self.assertEqual(game.computer_pairs, [])
        self.assertEqual(game.player_pairs, [])

    def test_game_clear(self):
        game = GoFish()
        game.reset()
        self.assertEqual(game.history, [])

    def test_get_wins(self):
        game = GoFish()
        self.assertEqual(game.get_wins(), 0)

    def test_get_total(self):
        game = GoFish()
        self.assertEqual(game.get_total(), 0)

    def test_output(self):
        game = GoFish()
        welcome_string = "***********************\n" \
                         "* Welcome to Go Fish! *\n" \
                         "***********************\n"
        instructs = "Enter a guess (2-10 or J-A) for Go Fish.\n"
        output = ("" if game.history != [] else welcome_string) + \
                 game.game_state + '\n\n' + \
                 ("Result of your last turn: " + game.evaluation + '\n' + "Result of Computer's turn: "
                  + game.comp_result if game.history != [] else "") + '\n' + \
                 game.get_hand() + '\n\n' + \
                 'Your Current Score: ' + str(game.player_score) + '\n' + \
                 'Computer Score: ' + str(game.computer_score) + '\n' + \
                 (instructs if game.game_state == 'GAME IN SESSION' else '') + \
                 '\nEnter "reset" to reset the current session, ' + \
                 'or "clear" to clear Go Fish\'s game history.'

        self.assertEqual(game.output(), output)

    def test_input(self):
        game = GoFish()
        game.input("A")

        game.input("reset")
        self.assertEqual(game.evaluation, "")

        game.input("A")
        game.input("clear")
        self.assertEqual(game.history, [])

        game.game_state = "Finished"
        self.assertEqual(game.input("A"), None)

        game.game_state = "GAME IN SESSION"
        game.input("J")
        self.assertEqual(game.history, ["J"])

    def test_get_score(self):
        game = GoFish()
        self.assertEqual(game.get_score(), 0)

    def test_check_game_over(self):
        game = GoFish()
        self.assertEqual(game.check_game_over(), False)
        self.assertEqual(game.check_game(), "Game over, tie")

    def test_clear(self):
        game = GoFish()
        game.clear
        self.assertEqual(game.history, [])

    def test_tie(self):
        game = GoFish()
        game.deck = []
        game.input("A")
        self.assertEqual(game.game_state, "Game over, tie")

    def test_game_over(self):
        game = GoFish()
        game.deck = []
        game.computer_pairs = ["A of Hearts.", "A of Diamonds."]
        game.player_hand = []
        game.player_guess("2")
        try:
            self.assertEqual("Computer wins", game.check_game())
        except AssertionError:
            self.assertEqual("Game over, tie", game.check_game())

    def test_player_win(self):
        game = GoFish()
        game.deck = []
        game.player_pairs = ["A of Hearts.", "A of Diamonds"]
        self.assertEqual(game.check_game(), "You win")

    def test_comp_win(self):
        game = GoFish()
        game.deck = []
        game.computer_pairs = ["A of Hearts.", "A of Diamonds"]
        self.assertEqual(game.check_game(), "Computer wins")

    def test_computer_win_after_guess(self):
        game = GoFish()
        game.deck = []
        game.computer_pairs = ["A of Hearts.", "A of Diamonds"]
        game.player_hand = []
        correct = "Guess correct, all cards of value " + "A" + " removed from computer hand and added to your hand\n"
        correct_and_player_win = "Guess correct, game over, you win\n"
        correct_and_computer_win = "Guess correct, game over, computer wins\n"
        guess = "A"
        tie = "Guess correct, game over, tie\n"
        self.assertEqual(game.process_player_guess(correct,
                                                   correct_and_computer_win,
                                                   correct_and_player_win, guess,
                                                   tie), game.res)

    def test_player_win_after_guess(self):
        game = GoFish()
        game.deck = []
        game.player_pairs = ["A of Hearts.", "A of Diamonds"]
        game.computer_hand = []
        correct = "Guess correct, all cards of value " + "A" + " removed from computer hand and added to your hand\n"
        correct_and_player_win = "Guess correct, game over, you win\n"
        correct_and_computer_win = "Guess correct, game over, computer wins\n"
        guess = "A"
        tie = "Guess correct, game over, tie\n"
        self.assertEqual(game.process_player_guess(correct,
                                                   correct_and_computer_win,
                                                   correct_and_player_win, guess,
                                                   tie), correct_and_player_win)

    def test_tie_after_guess(self):
        game = GoFish()
        game.deck = []
        correct = "Guess correct, all cards of value " + "A" + " removed from computer hand and added to your hand\n"
        correct_and_player_win = "Guess correct, game over, you win\n"
        correct_and_computer_win = "Guess correct, game over, computer wins\n"
        guess = ""
        tie = "Guess correct, game over, tie\n"
        game.player_pairs = []
        game.computer_pairs = []
        game.computer_hand = []
        game.player_hand = []
        ret = game.process_player_guess(correct, correct_and_computer_win, correct_and_player_win, guess, tie)
        self.assertEqual(ret, tie)

    def test_computer_guess_result(self):
        game = GoFish()
        game.deck = []
        self.assertEqual(game.computer_guess(""), False)
        game = GoFish()
        game.deck = []
        self.assertEqual(game.computer_guess(""), False)

    def test_clear(self):
        game = GoFish()
        game.clear()
        self.assertEqual(GoFish.all_histories, [])
        self.assertEqual(game.history, [])

    def test_get_pairs(self):
        game = GoFish()
        game.player_hand = []
        self.assertEqual(game.get_pairs(), "")
        game.player_pairs = [("A", "Hearts"), ("A", "Diamonds")]
        self.assertEqual(game.get_pairs(), "\nCurrent Pairs: A of Hearts. A of Diamonds. \n")
