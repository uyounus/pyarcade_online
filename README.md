# PyArcade Online

An online game arcade. This is the final project for
CMSC435, UMD, Spring 2020.

### How to run PyArcade Online
* **The Docker Way:**
To run _PyArcade Online_, we need to build the image of the game and then interactively run it using the following command:
<pre>
docker-compose run pyarcade
</pre>  

* **Without Installation:**
To run _PyArcade Online_ without installing it, run `python pyarcade/start.py
` from the project root folder.

* **With Installation:**
To install _PyArcade Online_, run `python setup.py
install` from the project root folder. Then, you can start playing from
 any directory by simply running `pyarcade`.  

### How to test PyArcade Online 

* **The Docker Way:**
To test _PyArcade Online_ and get _code coverage_, build and run the test files using the following single command:
<pre>
docker-compose -f test_pyarcade.yml up
</pre> 
To _close_ the docker container, run the following command:
<pre>
docker-compose -f test_pyarcade.yml down
</pre>

* **The Conventional Way:**
To test _PyArcade Online_, run the test files in the `tests/` folder. You can do so by running the following commands:  
<pre>
pip install -r requirements.txt
python setup.py test
</pre>  
For code coverage, run the following commands (after running the 2 commands above):  
<pre>
coverage run --include="pyarcade/*" setup.py test
coverage report -m
</pre>

### How to Access PyArcade Documentation
* **Accessing the Doc File:**
To access the documentation for all files of Pyarcade_Online, please navigate to the directory below from root folder:
<pre>
Documentation/html
</pre>
Please open the file with the name below, in a Web browser, to access the documentation:
<pre>
index.html
</pre>

### Video Tutorial of PyArcade Online (Third Release)
Click the image link below to watch a video tutorial for the third release
 of _PyArcade Online_.

[![Pyarcade_Online](http://img.youtube.com/vi/4Rpp_5xA4po/0.jpg)](http://www.youtube.com/watch?v=4Rpp_5xA4po "Tutorial: Release 3")

### Sprint 3 Effort Distribution (total 100%)

__Andrew Gordon: 25% effort__
1. Setup UI for viewing flight hours (represented as hours, minutes and seconds)
2. Created tests for viewing a users flight hours
3. Added functionality for a user to change their name
4. Created tests for a user changing their name
5. Fixed old tests to accommodate changes to the database (from local storage to rest api)
6. Tested the entire game and made multiple changes, fixing bugs found along the way

__Ophir Gal: 25% effort__
1. Implemented logic for tracking a user's "flight hours" (time spent) on PyArcade.
2. Set up the database on the AWS cloud.
3. Set up the REST API.
4. Configured CI/CD for REST API.
5. Wrote tests for REST API.
6. Modified project classes to use API.
7. Set up UI for viewing the leaderboard.
8. Ensured Mastermind is up to client's standards.
9. Made music cross-platform.

__Stephen Nelson Jr.: 25% effort__
1. Updated and improved Go Fish
2. Added UI and database option for deleting user logs
3. Increased code coverage for all games
4. Proofread documentation and checked PEP8 standards for all files
5. Added Sphinx requirements and files to Pyarcade Online
6. Worked with Usama to generate Sphinx Documentation


__Usama Younus: 25% effort__
1. Created Help screen feature for all games
2. Added tests for the Help screen
3. Refactored code and game tests, to move game instructions to help screen
4. Added the Mailbox feature to the game for messaging functionality
5. Added tests for the Mailbox, as well as, its UI
6. Refactored comments in all files with Stephen to generate Sphinx documents
7. Recorded and added the Tutorial video for Release 3
8. Updated Readme

### Sprint 2 Effort Distribution (total 100%)

__Andrew Gordon: 25% effort__
1. Added functionality to create a user profile
2. Created tests that achieved 90%+ coverage for account creation
3. Collaborated with Ophir to create a database for user accounts and scores
4. Updated UI to reflect the above

__Ophir Gal: 25% effort__  
1. Added login screen and its tests
2. Added scores screen and its tests
3. Added database class and its tests
4. Added ability to switch between games
5. Improved all games and helped teammates fix their issues
6. Made sure project was meeting requirements and being submitted on time
  
__Stephen Nelson Jr.: 25% effort__
1. Added difficulties to all games
2. Added scores to all games
3. Updated User Interface for all games
4. Fixed bugs and added tests for all game logic

__Usama Younus: 25% effort__
1. Setting up and troubleshooting the Docker Environment for remote deployment
2. Developed the Dockerfile for Packaging Dependencies
3. Created the Compose files for Testing and Deployment
4. Refactored to deploy & test pyarcade through a single command of user
5. Recorded and added the Tutorial video for Release 2
6. Updated the README for Release 2

### Sprint 1 Effort Distribution (total 100%)

__Ophir Gal: 25% effort__  
1. Persona/scenarios/stories of "Social Gamer".
2. Added "save and quit" functionality.
3. Integrated Go Fish into GameManager and View classes.
4. Updated Mastermind to display history.
5. Updated setup.py and created MANIFEST.in file.
6. Created README.
  
__Andrew Gordon: 25% effort__
1. Persona/scenarios/stories of “Casual Cameron”.
2. Updated UI for Minesweeper and Connect4.
3. Added documentation to Connect4.
4. Fixed bug in Connect4 (indexing out of bounds).
5. Co-authored README.
  
__Usama Younus: 25% effort__
1. Persona/scenarios/stories of “Beginner Gamer”.
2. Developed "pause/resume" functionality into pyarcade.
3. Added documentation for the functionality.
4. Recorded Video Tutorial for Pyarcade, linked to README.
4. Co-authored README.
  
__Stephen Nelson Jr.: 25% effort__
1. Persona/scenarios/stories of “Bill the Real Estate Agent”.
2. Did a completely fresh integration of Go Fish into Pyarcade.
3. Added tests for Go Fish to Pyarcade.
4. Co-authored README.